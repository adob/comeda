package de.unistuttgart.ipvs.comeda.sync;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.Mode;
import de.unistuttgart.ipvs.comeda.model.Note;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;


/**
 * Singleton class used as a functional interface between {@link Note}, {@link Conflict}
 * and the local database system.
 */
public class SyncSQLHelper extends SQLiteOpenHelper {

    private static SyncSQLHelper instance;

    private NoteDatabaseProperties noteDatabaseProperties;
    private String noteTable;
    private String conflictTable;


    /**
     * @param context {@link Context} the database system operates on
     * @param noteDatabaseProperties {@link NoteDatabaseProperties} of NoteDatabase which should
     * be modified.
     * @return A singleton {@link SyncSQLHelper} instance
     */
    public static SyncSQLHelper getInstance(Context context, NoteDatabaseProperties noteDatabaseProperties) {
        if (instance == null) {
            instance = new SyncSQLHelper(context, noteDatabaseProperties);

        } else {
            instance.noteDatabaseProperties = noteDatabaseProperties;
            instance.noteTable = "note_db_" + noteDatabaseProperties.getId();
            instance.conflictTable = "note_db_" + noteDatabaseProperties.getId() + "_conflict";
        }

        return instance;
    }

    private SyncSQLHelper(Context context, NoteDatabaseProperties noteDatabaseProperties) {
        super(context, "notes.db", null, 1);
        this.noteDatabaseProperties = noteDatabaseProperties;
        this.noteTable = "note_db_" + noteDatabaseProperties.getId();
        this.conflictTable = "note_db_" + noteDatabaseProperties.getId() + "_conflict";
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {}

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {}

    /**
     * Inserts the given {@link Note} with the given mode to the local database.
     *
     * @param note {@link Note} to insert
     * @param mode Mode to assign the Note with
     * @return Generated <code>CID</code> (Client side ID) by the local database system
     */
    public int insertNote(Note note, int mode) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("SID", note.getSid());
        values.put("MODE", mode);
        values.put("VERSION", note.getVersion());
        values.put("TIMESTAMP", note.getTimestamp());
        values.put("CONTENT", note.getContent());

        return (int) sqLiteDatabase.insert(noteTable, null, values);
    }

    /**
     * Updates the whole {@link Note} by <code>SID</code> (Server side ID) locally.
     *
     * @param note {@link Note} to be updated
     * @param mode Mode to assign the Note with
     */
    void updateNoteBySid(Note note, int mode) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("SID", note.getSid());
        values.put("MODE", mode);
        values.put("VERSION", note.getVersion());
        values.put("TIMESTAMP", note.getTimestamp());
        values.put("CONTENT", note.getContent());

        String where = "SID=?";
        String[] whereArgs = new String[] { String.valueOf(note.getSid()) };

        sqLiteDatabase.update(noteTable, values, where, whereArgs);
    }

    /**
     * Updates the whole {@link Note} by <code>CID</code> (Client side ID) locally.
     *
     * @param note {@link Note} to be updated
     * @param mode Mode to assign the Note with
     */
    public void updateNoteDataByCid(Note note, int mode) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("MODE", mode);
        values.put("VERSION", note.getVersion());
        values.put("TIMESTAMP", note.getTimestamp());
        values.put("CONTENT", note.getContent());

        String where = "CID=?";
        String[] whereArgs = new String[] { String.valueOf(note.getCid()) };

        sqLiteDatabase.update(noteTable, values, where, whereArgs);
    }

    /**
     * Marks the given {@link Note} with <code>Mode.DELETED</code> for further synchronization steps.
     *
     * @param note {@link Note} to be marked as DELETED
     */
    public void markAsDeleted(Note note) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("MODE", Mode.DELETED);

        String where = "CID=?";
        String[] whereArgs = new String[] { String.valueOf(note.getCid()) };

        sqLiteDatabase.update(noteTable, values, where, whereArgs);
    }

    /**
     * Deletes the given {@link Note} from the local database immediately.
     *
     * @param note {@link Note} to delete
     */
    public void deleteNote(Note note) {
        executeSQL("DELETE FROM " + noteTable + " WHERE CID = " + note.getCid());
    }

    /**
     * Deletes all Notes from the local database by the given list of <code>SID</code>.
     *
     * @param sidList List of <code>SID</code> of Notes which should be deleted
     */
    void deleteNotes(ArrayList<Integer> sidList) {
        for (Integer sid : sidList) {
            executeSQL("DELETE FROM " + noteTable + " WHERE SID = " + sid);
        }
    }

    /**
     * Deletes Notes which are marked as DELETED from the local database by the given list of
     * <code>SID</code>.
     *
     * @param sidList List of <code>SID</code> of Notes which should be deleted
     */
    void deleteMarkedNotes(ArrayList<Integer> sidList) {
        for (Integer sid : sidList) {
            executeSQL("DELETE FROM " + noteTable + " WHERE SID = " + sid +
                    " AND MODE = " + Mode.DELETED);
        }
    }

    /**
     * @return An ArrayList of {@link Note}s which are not marked as DELETED
     */
    public ArrayList<Note> getAllNotes() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<>();

        String sqlQuery = "SELECT SID, CID, VERSION, TIMESTAMP, CONTENT FROM " + noteTable +
                " WHERE MODE != " + Mode.DELETED + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Note note = new Note(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                        cursor.getInt(3), cursor.getString(4));
                notes.add(note);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return notes;
    }

    /**
     * @return A {@link Note} from local database which has the given <code>SID</code>.
     * <code>null</code> if the given <code>SID</code> does not exist.
     */
    public Note getNoteBySid(int sid) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String sqlQuery = "SELECT SID, CID, VERSION, TIMESTAMP, CONTENT FROM " + noteTable +
                " WHERE SID = " + sid + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            Note note = new Note(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                    cursor.getInt(3), cursor.getString(4));
            cursor.close();
            return note;
        }

        return null;
    }

    /**
     * @return A {@link Note} from local database which has the given <code>CID</code>.
     * <code>null</code> if the given <code>CID</code> does not exist.
     */
    public Note getNoteByCid(int cid) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String sqlQuery = "SELECT SID, CID, VERSION, TIMESTAMP, CONTENT FROM " + noteTable +
                " WHERE CID = " + cid + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            Note note = new Note(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                    cursor.getInt(3), cursor.getString(4));
            cursor.close();
            return note;
        }

        return null;
    }

    /**
     * @return Version number of the Note with the given <code>SID</code>
     */
    private int getVersionBySid(int sid) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String sqlQuery = "SELECT VERSION FROM " + noteTable + " WHERE SID = " + sid + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            int version = cursor.getInt(0);
            cursor.close();
            return version;
        }

        return 0;
    }

    /**
     * @return Version number of the Note with the given <code>CID</code>
     */
    public int getVersionByCid(int cid) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String sqlQuery = "SELECT VERSION FROM " + noteTable + " WHERE CID = " + cid + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            int version = cursor.getInt(0);
            cursor.close();
            return version;
        }

        return 0;
    }

    /**
     * @return An ArrayList of <code>SID</code> non-zero values which belong to Notes not
     * in OFFLINE mode.
     */
    ArrayList<Integer> getLocalSIDVector() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ArrayList<Integer> idVector = new ArrayList<>();

        String sqlQuery = "SELECT SID FROM " + noteTable + " WHERE SID != 0 AND MODE != " +
                Mode.OFFLINE + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                idVector.add(cursor.getInt(0));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return idVector;
    }

    /**
     * @return All {@link Note}s which are assigned to the given mode
     */
    ArrayList<Note> getNotesByMode(int mode) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<>();

        String sqlQuery = "SELECT SID, CID, VERSION, TIMESTAMP, CONTENT FROM " + noteTable +
                " WHERE MODE = " + mode + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Note note = new Note(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                        cursor.getInt(3), cursor.getString(4));
                notes.add(note);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return notes;
    }

    /**
     * @return An ArrayList of <code>SID</code> values which belong to Notes in RELEASED mode.
     */
    ArrayList<Integer> getReleasedSidList() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ArrayList<Integer> sidList = new ArrayList<>();

        String sqlQuery = "SELECT SID FROM " + noteTable + " WHERE MODE = " + Mode.RELEASED + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                sidList.add(cursor.getInt(0));

            } while (cursor.moveToNext());
        }

        cursor.close();
        return sidList;
    }

    /**
     * @return A mapping between <code>SID</code> and <code>VERSION</code> values of Notes which
     * belong to the given mode.
     */
    Map<Integer, Integer> getLocalSidVersionMap(int mode) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Map<Integer, Integer> sidVersionMap = new HashMap<>();

        String sqlQuery = "SELECT SID, VERSION FROM " + noteTable + " WHERE MODE = " + mode + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                sidVersionMap.put(cursor.getInt(0), cursor.getInt(1));

            } while (cursor.moveToNext());
        }

        cursor.close();
        return sidVersionMap;
    }

    /**
     * Updates the <code>SID</code> value by the given <code>CID</code> value.
     */
    void updateSid(int cid, int sid) {
        executeSQL("UPDATE " + noteTable + " SET SID = " + sid + " WHERE CID = " + cid + ";");
    }

    /**
     * Updates the <code>SID</code> and <code>MODE</code> columns by the given <code>CID</code>
     * value.
     */
    void updateSidAndMode(int cid, int sid, int mode) {
        executeSQL("UPDATE " + noteTable + " SET SID = " + sid + ", MODE = " + mode +
                " WHERE CID = " + cid + ";");
    }

    /**
     * Updates the <code>MODE</code> column by the given <code>SID</code> value.
     */
    void updateModeBySid(int sid, int mode) {
        executeSQL("UPDATE " + noteTable + " SET MODE = " + mode + " WHERE SID = " + sid + ";");
    }

    /**
     * Updates the <code>MODE</code> column by the given <code>CID</code> value.
     */
    void updateModeByCid(int cid, int mode) {
        executeSQL("UPDATE " + noteTable + " SET MODE = " + mode + " WHERE CID = " + cid + ";");
    }

    /**
     * Updates the <code>VERSION</code> column by the given <code>CID</code> value.
     */
    public void updateVersionByCid(int cid, int version) {
        executeSQL("UPDATE " + noteTable + " SET VERSION = " + version + " WHERE CID = " + cid + ";");
    }

    /**
     * @return Mode which is identified by the given <code>CID</code> value of a {@link Note}
     */
    public int getModeByCid(Note note) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        int mode = -1;

        String sqlQuery = "SELECT MODE FROM " + noteTable + " WHERE CID = " + note.getCid() + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            mode = cursor.getInt(0);
        }

        cursor.close();
        return mode;
    }

    /**
     * @return <code>true</code> if a Note with the given <code>CID</code> is present in local
     * database. Else <code>false</code>.
     */
    boolean isNotePresent(int cid) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String sqlQuery = "SELECT * FROM " + noteTable + " WHERE CID = " + cid + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);
        boolean present = cursor.moveToFirst();

        cursor.close();
        return present;
    }

    /**
     * @return A mapping between <code>CID</code> values and OFFLINE state. E.g. a tuple
     * <code>(1, true)</code> belongs to a Note with <code>CID=1</code> which is in OFFLINE mode.
     */
    public HashMap<Integer, Boolean> getCidOfflineMap() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        HashMap<Integer, Boolean> cidOfflineMap = new HashMap<>();

        String sqlQuery = "SELECT CID, MODE FROM " + noteTable + " WHERE MODE != " + Mode.DELETED + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                cidOfflineMap.put(cursor.getInt(0), (cursor.getInt(1) == Mode.OFFLINE));

            } while (cursor.moveToNext());
        }

        cursor.close();
        return cidOfflineMap;
    }

    /**
     * Inserts a conflict which persists of the given {@link Note} and a conflict type
     * (<code>Conflict.MODIFICATION</code> or <code>Conflict.PRESENCE</code>).
     * @param note Conflicting {@link Note}
     * @param conflictType <code>Conflict.MODIFICATION</code> or <code>Conflict.PRESENCE</code>
     */
    public void insertConflict(Note note, int conflictType) {
        if (!isNotePresent(note.getCid())) {
            int cid = insertNote(note, Mode.MODIFIED);
            note.setCid(cid);
        }

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        String sqlSelectQuery = "SELECT VERSION FROM " + conflictTable + " WHERE SID = " +
                note.getSid() + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlSelectQuery, null);

        if (!cursor.moveToFirst()) {
            ContentValues values = new ContentValues();

            values.put("SID", note.getSid());
            values.put("CID", note.getCid());
            values.put("TYPE", conflictType);
            values.put("VERSION", note.getVersion());
            values.put("TIMESTAMP", note.getTimestamp());
            values.put("CONTENT", note.getContent());

            sqLiteDatabase.insert(conflictTable, null, values);
        }

        cursor.close();
    }

    /**
     * @return <code>true</code> if the given {@link Note} is present in conflict table, else
     * <code>false</code>.
     */
    boolean isConflicting(Note note) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        String sqlSelectQuery = "SELECT * FROM " + conflictTable + " WHERE CID = " +
                note.getCid() + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlSelectQuery, null);

        boolean present = cursor.moveToFirst();

        cursor.close();
        return present;
    }

    /**
     * @return A mapping between <code>CID</code> values and {@link Conflict} objects
     */
    public HashMap<Integer, Conflict> getCidConflictMap() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        HashMap<Integer, Conflict> cidConflictMap = new HashMap<>();

        String sqlQuery = "SELECT SID, CID, TYPE, VERSION FROM " + conflictTable + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                int sid = cursor.getInt(0);
                int cid = cursor.getInt(1);
                int type = cursor.getInt(2);
                int localVersion = cursor.getInt(3);

                if (type == Conflict.MODIFICATION) {
                    // This returns the version of the note which has been synchronized with
                    // the server but is still in mode MODIFIED.
                    int remoteVersion = getVersionBySid(sid);
                    cidConflictMap.put(cid, new Conflict(sid, cid, type, localVersion, remoteVersion));

                } else if (type == Conflict.PRESENCE) {
                    cidConflictMap.put(cid, new Conflict(sid, cid, type, localVersion, 0));
                }
            } while (cursor.moveToNext());
        }

        cursor.close();
        return cidConflictMap;
    }

    /**
     * @return A conflicting {@link Note} selected by given <code>SID</code>. <code>null</code> if
     * not present in the conflict table.
     */
    public Note getLocalConflictingNote(int sid) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String sqlQuery = "SELECT SID, CID, VERSION, TIMESTAMP, CONTENT FROM " + conflictTable +
                " WHERE SID = " + sid + ";";

        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            Note note = new Note(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                    cursor.getInt(3), cursor.getString(4));

            cursor.close();
            return note;
        }

        return null;
    }

    /**
     * @return A conflicting {@link Note} selected by the given <code>SID</code> which may have been
     * updated. <code>null</code> if not present in the conflict table.
     */
    public Note getRemoteConflictingNote(int sid) {
        if (getLocalConflictingNote(sid) != null) {
            return getNoteBySid(sid);
        }
        return null;
    }

    /**
     * Marks the given {@link Note} as <code>MODIFIED</code> by <code>SID</code> value and
     * removes it from the conflict table.
     */
    public void resolveChangeConflict(Note note) {
        updateNoteBySid(note, Mode.MODIFIED);
        executeSQL("DELETE FROM " + conflictTable + " WHERE SID = " + note.getSid());
    }

    /**
     * Marks the given {@link Note} as <code>NEW</code> by <code>SID</code> if the parameter
     * <code>keep</code> is set to <code>true</code>.
     * If <code>keep=false</code>, the affected Note will also be removed the note table.
     * In both cases the {@link Conflict} will be removed from the conflict table.
     */
    public void resolvePresenceConflict(Conflict conflict, boolean keep) {
        if (keep) {
            updateModeBySid(conflict.getSid(), Mode.NEW);
        } else {
            executeSQL("DELETE FROM " + noteTable + " WHERE SID = " + conflict.getSid());
        }
        executeSQL("DELETE FROM " + conflictTable + " WHERE SID = " + conflict.getSid());
    }

    /**
     * Initialize the table structure for the local database.
     */
    public void createTables() {
        String sqlCommand = "CREATE TABLE IF NOT EXISTS " + noteTable + "(" +
                "SID INTEGER DEFAULT 0, " +
                "CID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "MODE INTEGER DEFAULT 0, " +
                "VERSION INTEGER DEFAULT 1, " +
                "TIMESTAMP INTEGER NULL, " +
                "CONTENT TEXT NULL" +
                ");";

        executeSQL(sqlCommand);

        sqlCommand = "CREATE TABLE IF NOT EXISTS " + conflictTable + "(" +
                "SID INTEGER DEFAULT 0, " +
                "CID INTEGER DEFAULT 0, " +
                "TYPE INTEGER DEFAULT 0, " +
                "VERSION INTEGER DEFAULT 1, " +
                "TIMESTAMP INTEGER NULL, " +
                "CONTENT TEXT NULL" +
                ");";

        executeSQL(sqlCommand);
    }

    /**
     * Delete note and conflict table.
     */
    public void deleteTables() {
        executeSQL("DROP TABLE IF EXISTS " + noteTable + ";");
        executeSQL("DROP TABLE IF EXISTS " + conflictTable + ";");
    }

    /**
     * Executes an arbitrary SQL command.
     *
     * @param sqlCommand SQL Command to execute
     */
    private void executeSQL(String sqlCommand) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(sqlCommand);
    }

    NoteDatabaseProperties getNoteDatabaseProperties() {
        return noteDatabaseProperties;
    }
}
