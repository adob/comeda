package de.unistuttgart.ipvs.comeda.exception;


/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.sync.SyncClient} when a secure connection
 * to the synchronization server cannot be established.
 */
public class SecureConnectionException extends Exception {
    public SecureConnectionException() {
        super();
    }
}
