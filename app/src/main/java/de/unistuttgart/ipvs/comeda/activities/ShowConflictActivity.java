package de.unistuttgart.ipvs.comeda.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.event.NoteAddedEvent;
import de.unistuttgart.ipvs.comeda.event.NoteEvent;
import de.unistuttgart.ipvs.comeda.event.NoteUpdatedEvent;
import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.Note;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.sync.SyncSQLHelper;
import de.unistuttgart.ipvs.comeda.view.NoteConflictTabFragment;
import de.unistuttgart.ipvs.comeda.view.ViewPagerAdapter;


/**
 * Provides a special view to resolve conflicts between two versions of a note.
 * A conflict can be resolved by modifying one of the versions which is visualized in an appropriate
 * tab and selecting the version which should be taken.
 */
public class ShowConflictActivity extends AppCompatActivity {

    private Conflict conflict;

    private SyncSQLHelper syncSQLHelper;

    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_conflict);

        Toolbar toolbar = findViewById(R.id.conflict_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Load the persistent Conflict object from another activity
        conflict = getIntent().getParcelableExtra(Conflict.EXTRA_KEY);

        NoteDatabaseProperties noteDatabaseProperties =
                getIntent().getParcelableExtra(NoteDatabaseProperties.EXTRA_KEY);
        syncSQLHelper = SyncSQLHelper.getInstance(this, noteDatabaseProperties);

        if (conflict.getType() == Conflict.MODIFICATION) {
            prepareModificationConflictView();

        } else if (conflict.getType() == Conflict.PRESENCE) {
            preparePresenceConflictView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_show_conflict, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_resolve_conflict:
                resolve();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        // keyEvent.getRepeatCount() is 0 only when the back button is pressed once.
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            finish();
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    /**
     * Uses the selected tab to get data for conflict resolution whereby the version is increased
     * by 1 and current timestamp is set. Furthermore a {@link NoteUpdatedEvent} is sent to
     * notify {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} about changes.
     */
    private void resolve() {
        int selectedTabIndex = tabLayout.getSelectedTabPosition();
        String resolvedContent =
                ((NoteConflictTabFragment) viewPagerAdapter.getItem(selectedTabIndex)).getContent();

        int version = Math.max(conflict.getLocalVersion(), conflict.getRemoteVersion());
        int timestamp = (int) (System.currentTimeMillis() / 1000L);

        Note note = syncSQLHelper.getNoteBySid(conflict.getSid());
        note.setVersion(version + 1);
        note.setTimestamp(timestamp);
        note.setContent(resolvedContent);
        syncSQLHelper.resolveChangeConflict(note);
        postEvent(new NoteUpdatedEvent(note));

        finish();
    }

    /**
     * Called only when MODIFICATION conflict is set as type.
     * Configures and fills the {@link TabLayout} with local and remote version of a note.
     */
    private void prepareModificationConflictView() {
        tabLayout = findViewById(R.id.conflict_tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(
                String.format(getResources().getString(R.string.local_version),
                        conflict.getLocalVersion())));
        tabLayout.addTab(tabLayout.newTab().setText(
                String.format(getResources().getString(R.string.remote_version),
                        conflict.getRemoteVersion())));

        viewPagerAdapter = new ViewPagerAdapter(
                getSupportFragmentManager(),
                tabLayout.getTabCount(),
                syncSQLHelper.getLocalConflictingNote(conflict.getSid()),
                syncSQLHelper.getRemoteConflictingNote(conflict.getSid()));

        final ViewPager viewPager = findViewById(R.id.conflict_view_pager);

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // Set callback for distinction between local and remote note.
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });
    }

    /**
     * Called only when PRESENCE conflict is set as type.
     * A popup with options for further proceed is shown.
     */
    private void preparePresenceConflictView() {
        DialogInterface.OnClickListener keepListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == DialogInterface.BUTTON_POSITIVE) {
                    SyncSQLHelper syncSQLHelper = ShowConflictActivity.this.syncSQLHelper;
                    Conflict conflict = ShowConflictActivity.this.conflict;
                    syncSQLHelper.resolvePresenceConflict(conflict, true);
                    postEvent(new NoteAddedEvent(syncSQLHelper.getNoteBySid(conflict.getSid())));
                    dialogInterface.dismiss();
                    finish();
                }
            }
        };

        DialogInterface.OnClickListener deleteListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == DialogInterface.BUTTON_NEGATIVE) {
                    SyncSQLHelper syncSQLHelper = ShowConflictActivity.this.syncSQLHelper;
                    Conflict conflict = ShowConflictActivity.this.conflict;
                    syncSQLHelper.resolvePresenceConflict(conflict, false);
                    dialogInterface.dismiss();
                    finish();
                }
            }
        };

        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == DialogInterface.BUTTON_NEUTRAL) {
                    dialogInterface.dismiss();
                    finish();
                }
            }
        };

        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle(getResources().getString(R.string.presence_conflict));
        dialog.setCancelable(false);
        dialog.setMessage(getResources().getString(R.string.changed_note_na_message));
        dialog.setButton(AlertDialog.BUTTON_POSITIVE,
                getResources().getString(R.string.keep), keepListener);
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.delete), deleteListener);
        dialog.setButton(AlertDialog.BUTTON_NEUTRAL,
                getResources().getString(R.string.cancel), cancelListener);

        dialog.show();
    }

    private void postEvent(NoteEvent event) {
        EventBus.getDefault().post(event);
    }
}
