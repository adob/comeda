package de.unistuttgart.ipvs.comeda.sync;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.unistuttgart.ipvs.comeda.automaton.State;
import de.unistuttgart.ipvs.comeda.automaton.SyncAutomaton;
import de.unistuttgart.ipvs.comeda.automaton.Transition;
import de.unistuttgart.ipvs.comeda.event.NoteAddedEvent;
import de.unistuttgart.ipvs.comeda.event.NoteDeletedEvent;
import de.unistuttgart.ipvs.comeda.event.NoteUpdatedEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UIInfoEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UINoteDeletedEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UINoteDeletionRefusedEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UIRefreshEvent;
import de.unistuttgart.ipvs.comeda.exception.FatalProtocolException;
import de.unistuttgart.ipvs.comeda.exception.NoSuchTransitionException;
import de.unistuttgart.ipvs.comeda.exception.ParseException;
import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.Constants;
import de.unistuttgart.ipvs.comeda.model.Mode;
import de.unistuttgart.ipvs.comeda.model.Note;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.stream.Stream;
import de.unistuttgart.ipvs.comeda.util.Composer;
import de.unistuttgart.ipvs.comeda.util.Parser;


/**
 * The core class for the implemented synchronization protocol. It pools {@link SyncAutomaton} and
 * {@link SyncSQLHelper} to facilitate correct synchronization between the local and remote database.
 *
 * {@link SyncJob}s are received asynchronously by the <code>EventBus</code> to fully uncouple
 * the user from any synchronization step.
 *
 * {@link Stream} is used for communication between this client and the sync server.
 */
public class SyncProcessor {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private SyncAutomaton syncAutomaton;
    private SyncSQLHelper syncSQLHelper;
    private Queue<SyncJob> jobQueue;
    private EventBus eventBus;

    private Stream stream;
    private boolean enabled;

    private FatalProtocolException currentProtocolException;

    SyncProcessor(SyncSQLHelper syncSQLHelper) {
        this.syncSQLHelper = syncSQLHelper;
        this.syncAutomaton = new SyncAutomaton();
        this.jobQueue = new ConcurrentLinkedQueue<>();
        this.eventBus = EventBus.getDefault();
        this.enabled = true;
    }

    /**
     * Periodically executed method by {@link SyncClient}. It analyses the current state of the
     * {@link SyncAutomaton} and performs dependant on the state synchronization operations
     * according to the sync protocol. It also sends, receives and applies {@link Transition}s to
     * switch between states.
     */
    void cycle() throws IOException, FatalProtocolException {
        State currentState = syncAutomaton.getCurrentState();

        if (currentState == State.START) {
            postTransition(Transition.CLIENT_RDB_CONNECT_REQ);
            receiveAndApplyTransition();

        } else if (currentState == State.CONNECTING) {
            NoteDatabaseProperties properties = syncSQLHelper.getNoteDatabaseProperties();
            postData(Composer.composeNoteDatabaseProperties(properties));
            receiveAndApplyTransition();

        } else if (currentState == State.CONNECTION_FAILED) {
            String errorMessage = receiveData();
            currentProtocolException = new FatalProtocolException(errorMessage);
            applyAndPostTransition(Transition.CLIENT_RDB_CONNECT_ABORT);

        } else if (currentState == State.PRIMARY_PULL) {
            eventBus.post(new UIInfoEvent(Constants.Messages.SYNC_STARTED));
            ArrayList<Integer> localSIDVector = syncSQLHelper.getLocalSIDVector();
            postData(Composer.composeIntVector(localSIDVector));

            ArrayList<Note> missingNotes = Parser.getParsedNoteArray(receiveDataArray());
            for (Note note : missingNotes) {
                syncSQLHelper.insertNote(note, Mode.RELEASED);
            }
            receiveAndApplyTransition();

        } else if (currentState == State.PRIMARY_PUSH) {
            ArrayList<Note> localNewNotes = syncSQLHelper.getNotesByMode(Mode.NEW);
            postDataArray(Composer.getComposedNoteArray(localNewNotes));

            Map<Integer, Integer> sidCidMap = Parser.parseIntMap(receiveData());

            for (Map.Entry<Integer, Integer> sidCidEntry : sidCidMap.entrySet()) {
                int sid = sidCidEntry.getKey();
                int cid = sidCidEntry.getValue();
                syncSQLHelper.updateSidAndMode(cid, sid, Mode.RELEASED);
            }

            try {
                applyAndPostTransition(Transition.CLIENT_PRIMARY_PUSH_COMPLETE);

            } catch (IOException e) {
                // Rollback
                // As the confirmation is not sent, the synchronized SID has to be reverted.
                // Otherwise the client would contain entries with SID != 0, which are not present
                // on the server side.
                for (Integer cid : sidCidMap.values()) {
                    syncSQLHelper.updateSidAndMode(cid, 0, Mode.NEW);
                }
                throw e;
            }


        } else if (currentState == State.PRIMARY_CLIENT_UPDATE) {
            Map<Integer, Integer> sidVersionMap = syncSQLHelper.getLocalSidVersionMap(Mode.RELEASED);
            postData(Composer.composeIntMap(sidVersionMap));

            ArrayList<Note> notes = Parser.getParsedNoteArray(receiveDataArray());

            for (Note note : notes) {
                syncSQLHelper.updateNoteBySid(note, Mode.RELEASED);
            }

            receiveAndApplyTransition();

        } else if (currentState == State.PRIMARY_SERVER_UPDATE) {
            ArrayList<Note> modifiedNotes = syncSQLHelper.getNotesByMode(Mode.MODIFIED);

            postDataArray(Composer.getComposedNoteArray(modifiedNotes));

            ArrayList<Integer> updatedSidList = Parser.parseIntVector(receiveData());

            for (Integer sid : updatedSidList) {
                syncSQLHelper.updateModeBySid(sid, Mode.RELEASED);
            }

            applyAndPostTransition(Transition.SERVER_PRIMARY_UPDATE_COMPLETE);

        } else if (currentState == State.PRIMARY_CLIENT_DELETE) {
            postData(Composer.composeIntVector(syncSQLHelper.getReleasedSidList()));

            ArrayList<Integer> toDeleteSidList = Parser.parseIntVector(receiveData());
            syncSQLHelper.deleteNotes(toDeleteSidList);

            applyAndPostTransition(Transition.CLIENT_PRIMARY_DELETE_COMPLETE);

        } else if (currentState == State.PRIMARY_SERVER_DELETE) {
            postData(Composer.composeIntMap(syncSQLHelper.getLocalSidVersionMap(Mode.DELETED)));
            ArrayList<Integer> sidToDeleteList = Parser.parseIntVector(receiveData());
            ArrayList<Note> notesToUndelete = Parser.getParsedNoteArray(receiveDataArray());

            syncSQLHelper.deleteMarkedNotes(sidToDeleteList);
            for (Note note : notesToUndelete) {
                syncSQLHelper.updateNoteBySid(note, Mode.RELEASED);
            }

            applyAndPostTransition(Transition.PRIMARY_SERVER_DELETE_COMPLETE);

        } else if (currentState == State.PRIMARY_CONFLICT_PULL) {
            Map<Integer, Integer> sidVersionMap = syncSQLHelper.getLocalSidVersionMap(Mode.MODIFIED);
            postData(Composer.composeIntMap(sidVersionMap));

            ArrayList<Note> serverConflictingNotes = Parser.getParsedNoteArray(receiveDataArray());

            for (Note serverConflictingNote : serverConflictingNotes) {
                Note clientConflictingNote = syncSQLHelper.getNoteBySid(serverConflictingNote.getSid());
                syncSQLHelper.updateNoteBySid(serverConflictingNote, Mode.MODIFIED);
                syncSQLHelper.insertConflict(clientConflictingNote, Conflict.MODIFICATION);
            }

            ArrayList<Integer> missingSidList = Parser.parseIntVector(receiveData());

            for (Integer sid : missingSidList) {
                Note conflictingNote = syncSQLHelper.getNoteBySid(sid);
                syncSQLHelper.insertConflict(conflictingNote, Conflict.PRESENCE);
            }

            // Send an event to refresh the UI as primary sync is done after this transition
            eventBus.post(new UIRefreshEvent());
            receiveAndApplyTransition();

        } else if (currentState == State.STANDBY) {
            receiveAndApplyTransition();

        } else if (currentState == State.ACTIVE) {
            SyncJob syncJob = jobQueue.peek();

            if (syncJob == null) {
                idle();
                applyAndPostTransition(Transition.CLIENT_ACK);

            } else {
                switch (syncJob.getOperation()) {
                    case ADD:
                        if (syncSQLHelper.isNotePresent(syncJob.getNote().getCid()) &&
                                syncSQLHelper.getModeByCid(syncJob.getNote()) == Mode.NEW) {
                            applyAndPostTransition(Transition.DATA_ADDED);

                        } else {
                            // The note probably has been uploaded by primary push
                            LOGGER.log(Level.INFO, "Note with CID " + syncJob.getNote().getCid() +
                                    " is not present or is not NEW (anymore).");
                            jobQueue.poll();
                        }
                        break;

                    case UPDATE:
                        if (syncSQLHelper.isNotePresent(syncJob.getNote().getCid())) {
                            applyAndPostTransition(Transition.DATA_UPDATED);

                        } else {
                            // Changes probably have already been sent by primary update push
                            LOGGER.log(Level.INFO, "Note with CID " + syncJob.getNote().getCid() +
                                    " is not present.");
                            jobQueue.poll();
                        }
                        break;

                    case DELETE:
                        if (syncSQLHelper.isNotePresent(syncJob.getNote().getCid())) {
                            applyAndPostTransition(Transition.DATA_DELETED);

                        } else {
                            // Delete has already been performed.
                            LOGGER.log(Level.INFO, "Note with CID " + syncJob.getNote().getCid() +
                                    " is not present.");
                            jobQueue.poll();
                        }
                        break;
                }
            }

        } else if (currentState == State.IMMEDIATE_PUSH) {
            SyncJob syncJob = jobQueue.poll();
            Note capturedNote = syncJob.getNote();

            postData(Composer.composeNote(capturedNote));

            int sid = Parser.parseInt(receiveData());

            Note currentNote = syncSQLHelper.getNoteByCid(capturedNote.getCid());

            syncSQLHelper.updateSid(capturedNote.getCid(), sid);

            if (syncSQLHelper.getModeByCid(currentNote) != Mode.DELETED) {
                syncSQLHelper.updateSidAndMode(capturedNote.getCid(), sid, Mode.RELEASED);
            }

            eventBus.post(new UIRefreshEvent());
            applyAndPostTransition(Transition.IMMEDIATE_PUSH_COMPLETE);

        } else if (currentState == State.IMMEDIATE_PULL) {
            Note note = Parser.parseNote(receiveData());
            syncSQLHelper.insertNote(note, Mode.RELEASED);
            eventBus.post(new UIRefreshEvent());
            receiveAndApplyTransition();

        } else if (currentState == State.IMMEDIATE_UPDATE_PUSH) {
            SyncJob syncJob = jobQueue.peek();
            Note capturedNote = syncJob.getNote();

            if (capturedNote.getSid() == 0) {
                // case when the local note is not synchronized yet
                capturedNote.setSid(syncSQLHelper.getNoteByCid(capturedNote.getCid()).getSid());
            }

            postData(Composer.composeNote(capturedNote));

            boolean updated = Parser.parseBoolean(receiveData());
            boolean notUpdatableNotePresent = Parser.parseBoolean(receiveData());

            if (updated) {
                syncSQLHelper.updateModeBySid(capturedNote.getSid(), Mode.RELEASED);

            } else if (notUpdatableNotePresent) {
                Note notUpdatableNote = Parser.parseNote(receiveData());
                syncSQLHelper.updateNoteBySid(notUpdatableNote, Mode.MODIFIED);
                syncSQLHelper.insertConflict(capturedNote, Conflict.MODIFICATION);

            } else {
                syncSQLHelper.insertConflict(capturedNote, Conflict.PRESENCE);
            }

            eventBus.post(new UIRefreshEvent());
            jobQueue.poll();
            receiveAndApplyTransition();

        } else if (currentState == State.IMMEDIATE_UPDATE_PULL) {
            Note receivedNote = Parser.parseNote(receiveData());
            Note localNote = syncSQLHelper.getNoteBySid(receivedNote.getSid());

            if (localNote != null) {
                // This case occurs when the same synchronized note is modified _before_
                // IMMEDIATE_UPDATE_PULL pulls down new changes from another client.
                if (localNote.getVersion() >= receivedNote.getVersion()) {
                    syncSQLHelper.insertConflict(localNote, Conflict.MODIFICATION);
                }
                syncSQLHelper.updateNoteBySid(receivedNote, Mode.RELEASED);
                eventBus.post(new UIRefreshEvent());
            }
            receiveAndApplyTransition();

        } else if (currentState == State.IMMEDIATE_DELETE_PUSH) {
            SyncJob syncJob = jobQueue.poll();
            Note capturedNoteToDelete = syncJob.getNote();
            Note currentNoteToDelete = syncSQLHelper.getNoteByCid(syncJob.getNote().getCid());

            if (currentNoteToDelete.getVersion() <= capturedNoteToDelete.getVersion() && currentNoteToDelete.getSid() != 0) {
                postData(Composer.composeBoolean(true));
                postData(Composer.composeNote(currentNoteToDelete));
                if (syncSQLHelper.getModeByCid(currentNoteToDelete) != Mode.OFFLINE) {
                    // Delete only when not desired for offline usage
                    syncSQLHelper.deleteNote(currentNoteToDelete);
                }

            } else if (currentNoteToDelete.getSid() == 0) {
                // The note was not synchronized. Remote DELETE with SID=0 is unnecessary but would not harm.
                postData(Composer.composeBoolean(false));

            } else {
                // In this case the note which should be deleted has been updated by the server.
                // Unmark note as DELETED and notify UI.
                postData(Composer.composeBoolean(false));
                syncSQLHelper.updateModeByCid(capturedNoteToDelete.getCid(), Mode.RELEASED);
                eventBus.post(new UINoteDeletionRefusedEvent(currentNoteToDelete));
            }

            eventBus.post(new UIRefreshEvent());
            receiveAndApplyTransition();

        } else if (currentState == State.IMMEDIATE_DELETE_PULL) {
            Note receivedNoteToDelete = Parser.parseNote(receiveData());
            Note currentNoteToDelete = syncSQLHelper.getNoteBySid(receivedNoteToDelete.getSid());

            if (currentNoteToDelete != null && !syncSQLHelper.isConflicting(currentNoteToDelete)) {
                if (currentNoteToDelete.getVersion() <= receivedNoteToDelete.getVersion()) {
                    if (syncSQLHelper.getModeByCid(currentNoteToDelete) != Mode.OFFLINE) {
                        syncSQLHelper.deleteNote(currentNoteToDelete);
                        eventBus.post(new UIRefreshEvent());
                        eventBus.post(new UINoteDeletedEvent(currentNoteToDelete));
                    }
                    // Otherwise the note has been marked for offline usage simultaneously.
                    // Do note delete in this case.

                } else {
                    // The note has been updated locally but deleted by the server.
                    syncSQLHelper.insertConflict(currentNoteToDelete, Conflict.PRESENCE);
                    eventBus.post(new UIRefreshEvent());
                }
            }

            receiveAndApplyTransition();

        } else if (currentState == State.ERROR_END) {
            enabled = false; // Disable processor due to error state
            throw currentProtocolException;
        }
    }

    /**
     * Receives and applies a {@link Transition} for the local {@link SyncAutomaton}.
     */
    private void receiveAndApplyTransition() throws IOException, FatalProtocolException {
        try {
            Transition transition = Parser.parseTransition(receive());
            syncAutomaton.apply(transition);

        } catch (ParseException | NoSuchTransitionException e) {
            throw new FatalProtocolException(e);
        }
    }

    /**
     * Applies the given {@link Transition} locally before it is sent to the sync server.
     * @param transition {@link Transition} which should be applied and sent
     */
    private void applyAndPostTransition(Transition transition) throws IOException {
        syncAutomaton.apply(transition);
        postTransition(transition);
    }

    /**
     * Applies the given {@link Transition} locally.
     * @param transition {@link Transition} which should be applied
     */
    private void applyTransition(Transition transition) {
        syncAutomaton.apply(transition);
    }

    /**
     * Receives raw data from the {@link Stream}
     * @return Raw data as string
     */
    private String receive() throws IOException {
        return stream.read();
    }

    /**
     * Receives data which has been prepared on the server side. E.g. Composed Maps, ArrayLists or
     * {@link Note}s.
     *
     * @return Composed data from the server side
     */
    private String receiveData() throws FatalProtocolException {
        try {
            return Parser.parseDataLine(receive());
        } catch (IOException | ParseException e) {
            throw new FatalProtocolException(e);
        }
    }

    /**
     * Receives multiple objects which have been composed on the server side. E.g.
     * An ArrayList of {@link Note}s.
     *
     * @return Composed data array from the server side
     */
    private ArrayList<String> receiveDataArray() throws IOException, FatalProtocolException {
        ArrayList<String> dataArray = new ArrayList<>();
        int size = Parser.parseInt(receiveData());

        for (int i = 0; i < size; i++) {
            dataArray.add(receiveData());
        }

        return dataArray;
    }

    /**
     * Receives a transition from the sync server.
     *
     * @return received {@link Transition}
     */
    private Transition receiveTransition() throws FatalProtocolException {
        try {
            return Parser.parseTransition(receive());
        } catch (IOException | ParseException | NoSuchTransitionException e) {
            throw new FatalProtocolException(e);
        }
    }

    /**
     * Posts raw data to the sync server.
     *
     * @param data Data to be posted
     */
    private void postData(String data) throws IOException {
        stream.write(Composer.composeDataLine(data));
    }

    /**
     * Posts an ArrayList of raw data to the sync server.
     *
     * @param dataArray Raw data to be posted
     */
    private void postDataArray(ArrayList<String> dataArray) throws IOException {
        postData(Composer.composeInt(dataArray.size()));

        for (String line : dataArray) {
            postData(line);
        }
    }

    /**
     * Posts a {@link Transition} to the sync server.
     *
     * @param transition {@link Transition} to be posted
     */
    private void postTransition(Transition transition) throws IOException {
        stream.write(Composer.composeTransition(transition));
    }

    /**
     * Listening method for {@link NoteAddedEvent}. Creates and enqueues a new {@link SyncJob} with
     * ADD operation.
     */
    @Subscribe
    public void listen(NoteAddedEvent event) {
        jobQueue.offer(new SyncJob(event.getNote(), SyncJob.Operation.ADD));
    }

    /**
     * Listening method for {@link NoteUpdatedEvent}. Creates and enqueues a new {@link SyncJob}
     * with UPDATE operation. If several equivalent {@link SyncJob}s are already enqueued, the
     * maximal version number of the {@link Note} is increased by one for the {@link Note} within
     * the current {@link SyncJob}.
     */
    @Subscribe
    public void listen(NoteUpdatedEvent event) {
        Note note = event.getNote();
        SyncJob syncJob = new SyncJob(note, SyncJob.Operation.UPDATE);
        ArrayList<SyncJob> allSyncJobs = new ArrayList<>(jobQueue);
        int maxVersion = note.getVersion();

        if (syncSQLHelper.getModeByCid(note) == Mode.NEW) {
            // The note has been updated while it was NEW
            maxVersion++;
        }

        for (SyncJob equivalentSyncJob : allSyncJobs) {
            int version = equivalentSyncJob.getNote().getVersion();
            if (equivalentSyncJob.equals(syncJob) && version >= maxVersion) {
                maxVersion = version + 1;
            }
        }

        syncSQLHelper.updateVersionByCid(note.getCid(), maxVersion);
        note.setVersion(maxVersion);
        jobQueue.offer(syncJob);
    }

    /**
     * Listening method for {@link NoteDeletedEvent}. Creates and enqueues a new {@link SyncJob} with
     * DELETE operation.
     */
    @Subscribe
    public void listen(NoteDeletedEvent event) {
        jobQueue.offer(new SyncJob(event.getNote(), SyncJob.Operation.DELETE));
    }

    void setStream(Stream stream) {
        this.stream = stream;
    }

    /**
     * @return <code>true</code> if the {@link SyncProcessor} is still enabled, else <code>false</code>.
     */
    boolean isEnabled() {
        return enabled;
    }

    /**
     * Disables {@link SyncProcessor} after the last state is worked off.
     */
    void disable() {
        this.enabled = false;
    }

    /**
     * Closes all streams.
     */
    void closeStreams() throws IOException {
        stream.close();
    }

    /**
     * Used to reduce data throughput to the sync server. This is used in ACTIVE state before
     * switching to the STANDBY state whereby the <code>jobQueue</code> is empty.
     */
    private void idle() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) { /* do nothing */}
    }

    /**
     * Clears the <code>jobQueue</code> and resets the internal {@link SyncAutomaton}.
     */
    void reset() {
        jobQueue.clear();
        syncAutomaton = new SyncAutomaton();
    }
}
