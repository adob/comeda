package de.unistuttgart.ipvs.comeda.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Used to represent a conflict between two {@link Note}s.
 */
public class Conflict implements Parcelable {

    public static final String EXTRA_KEY = "k1u09mbc5";

    public static final int MODIFICATION = 0;
    public static final int PRESENCE = 1;

    private int sid;
    private int cid;
    private int type;
    private int localVersion;
    private int remoteVersion;

    public Conflict(int sid, int cid, int type, int localVersion, int remoteVersion) {
        this.sid = sid;
        this.cid = cid;
        this.type = type;
        this.localVersion = localVersion;
        this.remoteVersion = remoteVersion;
    }

    private Conflict(Parcel in) {
        sid = in.readInt();
        cid = in.readInt();
        type = in.readInt();
        localVersion = in.readInt();
        remoteVersion = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sid);
        dest.writeInt(cid);
        dest.writeInt(type);
        dest.writeInt(localVersion);
        dest.writeInt(remoteVersion);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Conflict> CREATOR = new Creator<Conflict>() {
        @Override
        public Conflict createFromParcel(Parcel in) {
            return new Conflict(in);
        }

        @Override
        public Conflict[] newArray(int size) {
            return new Conflict[size];
        }
    };

    public int getSid() {
        return sid;
    }

    public int getType() {
        return type;
    }

    public int getLocalVersion() {
        return localVersion;
    }

    public int getRemoteVersion() {
        return remoteVersion;
    }
}
