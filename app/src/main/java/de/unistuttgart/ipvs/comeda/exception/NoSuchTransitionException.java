package de.unistuttgart.ipvs.comeda.exception;


/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.util.Parser} when the required transition is not
 * enumerated in {@link de.unistuttgart.ipvs.comeda.automaton.Transition}.
 */
public class NoSuchTransitionException extends Exception {
    public NoSuchTransitionException() {
        super("The given transition does not exist.");
    }
}
