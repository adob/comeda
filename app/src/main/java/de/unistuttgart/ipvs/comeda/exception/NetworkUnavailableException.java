package de.unistuttgart.ipvs.comeda.exception;


/**
 * Thrown when the internet connection of the mobile device is not available.
 */
public class NetworkUnavailableException extends Exception {
    public NetworkUnavailableException() {
        super();
    }
}
