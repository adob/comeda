package de.unistuttgart.ipvs.comeda.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.EditText;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.model.SyncServerProperties;
import de.unistuttgart.ipvs.comeda.sql.PropertiesSQLHelper;

/**
 * Provides the possibility to adjust main settings of this application. At this time the hostname
 * of the SyncServer and the port are available to change.
 */
public class MainSettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        PropertiesSQLHelper propertiesSQLHelper = new PropertiesSQLHelper(this);
        SyncServerProperties syncServerProperties = propertiesSQLHelper.getSyncServerProperties();

        EditText hostEditText = findViewById(R.id.syncserv_host_input);
        hostEditText.setText(syncServerProperties.getHost());

        EditText portEditText = findViewById(R.id.syncserv_port_input);
        portEditText.setText(syncServerProperties.getPort());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        // keyEvent.getRepeatCount() is 0 only when the back button is pressed once.
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            updateSyncServerProperties();
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // A explicit save button is not provided. Therefore the changes are going to be saved
        // on exit.
        if (item.getItemId() == android.R.id.home) {
            updateSyncServerProperties();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Persists entered data as {@link SyncServerProperties} using {@link PropertiesSQLHelper}.
     */
    private void updateSyncServerProperties() {
        EditText hostEditText = findViewById(R.id.syncserv_host_input);
        EditText portEditText = findViewById(R.id.syncserv_port_input);

        String host = hostEditText.getText().toString();
        String port = portEditText.getText().toString();

        SyncServerProperties syncServerProperties = new SyncServerProperties(host, port);

        PropertiesSQLHelper dbHelper = new PropertiesSQLHelper(this);
        dbHelper.updateSyncServerProperties(syncServerProperties);

        finish();
    }

}
