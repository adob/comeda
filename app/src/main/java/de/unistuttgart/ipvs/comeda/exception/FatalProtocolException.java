package de.unistuttgart.ipvs.comeda.exception;


/**
 * Thrown when the synchronization protocol has been violated and therefore it could not be resumed.
 */
public class FatalProtocolException extends Exception {

    public FatalProtocolException(Exception e) {
        super(e);
    }

    public FatalProtocolException(String message) {
        super(message);
    }
}
