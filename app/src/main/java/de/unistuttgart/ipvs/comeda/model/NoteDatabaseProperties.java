package de.unistuttgart.ipvs.comeda.model;


import android.os.Parcel;
import android.os.Parcelable;


/**
 * Represents properties of a note database which is saved in the local database system.
 * The storing is handled by {@link de.unistuttgart.ipvs.comeda.sql.PropertiesSQLHelper}
 * and the transfer to the server by {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor}.
 */
public class NoteDatabaseProperties implements Parcelable {

    public static final String EXTRA_KEY = "9oa3j0h1h4";

    private int id;
    private String name;
    private String host;
    private String port;
    private String database;
    private String username;
    private String password;

    public NoteDatabaseProperties(String name, String host, String port, String database, String username, String password) {
        this(0, name, host, port, database, username, password);
    }

    public NoteDatabaseProperties(int id, String name, String host, String port, String database, String username, String password) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    protected NoteDatabaseProperties(Parcel in) {
        id = in.readInt();
        name = in.readString();
        host = in.readString();
        port = in.readString();
        database = in.readString();
        username = in.readString();
        password = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(host);
        dest.writeString(port);
        dest.writeString(database);
        dest.writeString(username);
        dest.writeString(password);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NoteDatabaseProperties> CREATOR = new Creator<NoteDatabaseProperties>() {
        @Override
        public NoteDatabaseProperties createFromParcel(Parcel in) {
            return new NoteDatabaseProperties(in);
        }

        @Override
        public NoteDatabaseProperties[] newArray(int size) {
            return new NoteDatabaseProperties[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof NoteDatabaseProperties) {
            NoteDatabaseProperties otherProperties = (NoteDatabaseProperties) other;
            return name.equals(otherProperties.name) &&
                    host.equals(otherProperties.host) &&
                    port.equals(otherProperties.port) &&
                    database.equals(otherProperties.database) &&
                    username.equals(otherProperties.username) &&
                    password.equals(otherProperties.password);
        }
        return false;
    }
}
