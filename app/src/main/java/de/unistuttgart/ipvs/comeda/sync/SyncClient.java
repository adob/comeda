package de.unistuttgart.ipvs.comeda.sync;


import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;

import de.unistuttgart.ipvs.comeda.event.ui.ErrorEvent;
import de.unistuttgart.ipvs.comeda.exception.FatalProtocolException;
import de.unistuttgart.ipvs.comeda.exception.NetworkUnavailableException;
import de.unistuttgart.ipvs.comeda.exception.SecureConnectionException;
import de.unistuttgart.ipvs.comeda.model.Constants;
import de.unistuttgart.ipvs.comeda.ssl.SecureClientConnector;
import de.unistuttgart.ipvs.comeda.stream.Stream;
import de.unistuttgart.ipvs.comeda.stream.StreamReader;
import de.unistuttgart.ipvs.comeda.stream.StreamWriter;
import de.unistuttgart.ipvs.comeda.util.Networking;


/**
 * This singleton class represents the background service for the synchronization of the local
 * note database. It performs several checks and makes sure that all requirements for a
 * synchronization are met. Furthermore it clocks the {@link SyncProcessor} and is able to
 * end the synchronization in error case.
 */
public class SyncClient extends Thread {

    private static SyncClient instance;

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private SecureClientConnector secureClientConnector;
    private SyncProcessor syncProcessor;

    private ErrorEvent currentErrorEvent;

    private EventBus eventBus;

    private boolean retry;


    private SyncClient() {
        eventBus = EventBus.getDefault();
    }

    @Override
    public void run() {
        while (retry) {
            try {
                checkNetworkAvailability();
                establishSecureConnection();
                SSLSocket sslSocket = secureClientConnector.getSslSocket();

                Stream stream = new Stream(
                        new StreamReader(sslSocket.getInputStream()),
                        new StreamWriter(sslSocket.getOutputStream()));

                syncProcessor.setStream(stream);

                LOGGER.log(Level.INFO, "Synchronization started.");
                eventBus.register(syncProcessor);
                currentErrorEvent = null;

                // perform cycles without delays as long as the SyncProcessor is enabled
                // and this thread is not interrupted.
                while (syncProcessor.isEnabled() && !isInterrupted()) {
                    syncProcessor.cycle();
                }

                syncProcessor.closeStreams();
                sslSocket.close();
                LOGGER.log(Level.INFO, "Synchronization terminated.");

            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Connection lost. Retry to recover the connection...");
                idle(); // prevent CPU overload

            } catch (NetworkUnavailableException e) {
                LOGGER.log(Level.WARNING, "Network is not available. Waiting for connection...");
                idle(); // prevent CPU overload
                postOnce(new ErrorEvent(Constants.Titles.CONNECTION_ERROR,
                        Constants.Messages.NO_NETWORK));

            } catch (SecureConnectionException e) {
                retry = false;
                LOGGER.log(Level.SEVERE,
                        "Unable to establish a secure connection to the server. Giving up.");
                postOnce(new ErrorEvent(Constants.Titles.CONNECTION_ERROR,
                        Constants.Messages.CONNECTION_FAILED));

            } catch (FatalProtocolException e) {
                retry = false;
                LOGGER.log(Level.SEVERE, "The sync protocol has been violated.");
                postOnce(new ErrorEvent(Constants.Titles.PROTOCOL_ERROR,
                        Constants.Messages.PROTOCOL_VIOLATED + "\n\n" + e.getMessage()));
            }

            if (eventBus.isRegistered(syncProcessor)) {
                eventBus.unregister(syncProcessor);
            }
        }
    }

    /**
     * Establishes and awaits a new connection using {@link SecureClientConnector}.
     */
    private void establishSecureConnection() throws SecureConnectionException {
        try {
            syncProcessor.reset();
            secureClientConnector.connect();
            secureClientConnector.awaitSecureConnection();
        } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException |
                KeyManagementException | IOException e) {
            throw new SecureConnectionException();
        }
    }

    /**
     * Check the main availability of the internet connection.
     *
     * @throws NetworkUnavailableException if the internet connection is not available
     */
    private void checkNetworkAvailability() throws NetworkUnavailableException {
        if (!Networking.isNetworkAvailable()) {
            throw new NetworkUnavailableException();
        }
    }

    /**
     * Posts an {@link ErrorEvent} just once. For resetting <code>currentErrorEvent</code> has to
     * be set to <code>null</code>.
     *
     * @param event which should be posted
     */
    private void postOnce(ErrorEvent event) {
        if (currentErrorEvent == null || !currentErrorEvent.equals(event)) {
            currentErrorEvent = event;
            eventBus.post(event);
        }
    }

    /**
     * Quits the synchronization service.
     */
    public void quit() {
        retry = false;
        syncProcessor.disable();
    }

    /**
     * Used for periodic attempts to establish a secure connection to the sync server.
     * It prevents the CPU overload.
     */
    private void idle() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) { /* do nothing */ }
    }

    /**
     * Initializes the singleton class with new {@link SecureClientConnector} and
     * {@link SyncSQLHelper}.
     */
    public void init(SecureClientConnector secureClientConnector, SyncSQLHelper syncSQLHelper) {
        this.secureClientConnector = secureClientConnector;
        this.syncProcessor = new SyncProcessor(syncSQLHelper);
        this.retry = true;
    }

    /**
     * @return The instance of this singleton class
     */
    public static SyncClient getInstance() {
        if (instance == null) {
            instance = new SyncClient();
        }
        return instance;
    }

    /**
     * Destroys the currently set instance.
     */
    public static void destroyInstance() {
        instance = null;
    }

}
