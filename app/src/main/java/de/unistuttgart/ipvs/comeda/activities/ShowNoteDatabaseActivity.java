package de.unistuttgart.ipvs.comeda.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.event.ui.ErrorEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UIInfoEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UINoteDeletionRefusedEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UIRefreshEvent;
import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.KeystoreDefaults;
import de.unistuttgart.ipvs.comeda.model.Note;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.model.SecureConnectionProperties;
import de.unistuttgart.ipvs.comeda.model.SyncServerProperties;
import de.unistuttgart.ipvs.comeda.ssl.SecureClientConnector;
import de.unistuttgart.ipvs.comeda.sync.SyncClient;
import de.unistuttgart.ipvs.comeda.sync.SyncSQLHelper;
import de.unistuttgart.ipvs.comeda.view.ErrorDialog;
import de.unistuttgart.ipvs.comeda.view.RecyclerViewAdapter;
import de.unistuttgart.ipvs.comeda.view.RecyclerViewListener;


/**
 * Activity which uses RecyclerView to shows all available notes from the local database.
 * It redirects to ShowNoteActivity when a note is selected.
 */
public class ShowNoteDatabaseActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private NoteDatabaseProperties noteDatabaseProperties;
    private SyncClient syncClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_note_database);

        noteDatabaseProperties = getIntent().getParcelableExtra(NoteDatabaseProperties.EXTRA_KEY);
        SyncServerProperties syncServerProperties =
                getIntent().getParcelableExtra(SyncServerProperties.EXTRA_KEY);


        setTitle(noteDatabaseProperties.getName());

        Toolbar toolbar = findViewById(R.id.note_database_settings_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Prepare secure connection to the server
        SecureConnectionProperties scp = new SecureConnectionProperties(
                syncServerProperties.getHost(),
                syncServerProperties.getPortAsInt(),
                getResources().openRawResource(R.raw.client),
                KeystoreDefaults.getKeystorePassword(),
                getFilesDir().getPath());

        SecureClientConnector secureClientConnector = new SecureClientConnector(scp);
        SyncSQLHelper syncSQLHelper = SyncSQLHelper.getInstance(this, noteDatabaseProperties);

        syncClient = SyncClient.getInstance();
        syncClient.init(secureClientConnector, syncSQLHelper);
        syncClient.start();

        // Register to the EventBus to receive changes made by SyncProcessor
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_show_note_database, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add_note) {
            Intent intent = new Intent(ShowNoteDatabaseActivity.this, ShowNoteActivity.class);
            intent.putExtra(ShowNoteActivity.EXTRA_OPTION_KEY, ShowNoteActivity.EXTRA_OPTION_ADD);
            intent.putExtra(NoteDatabaseProperties.EXTRA_KEY, noteDatabaseProperties);
            ShowNoteDatabaseActivity.this.startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        syncClient.quit();
        SyncClient.destroyInstance();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Listening method which receives an ErrorEvent when it is posted on the EventBus.
     * It shows a popup with received error message.
     * @param event Received ErrorEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listen(ErrorEvent event) {
        new ErrorDialog(ShowNoteDatabaseActivity.this, event.getTitle(), event.getMessage(),
                ErrorDialog.NEUTRAL_DIALOG).show();
    }

    /**
     * Listening method which receives an UIInfoEvent when it is posted on the EventBus.
     * It shows a toast with an information (e.g. status).
     * @param event Received UIInfoEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listen(UIInfoEvent event) {
        Toast.makeText(this, event.getMessage(), Toast.LENGTH_LONG).show();
    }

    /**
     * Listening method which receives an UIRefreshEvent when it is posted on the EventBus.
     * It adapts the ListView to the current state of the local database.
     * @param event Received UIRefreshEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listen(UIRefreshEvent event) {
        updateListView();
    }

    /**
     * Listening method which receives an special UINoteDeletionRefusedEvent when it is posted
     * on the EventBus. It notifies the user that the removal of a note was not successful.
     * @param event Received UINoteDeletionRefusedEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listen(UINoteDeletionRefusedEvent event) {
        new ErrorDialog(ShowNoteDatabaseActivity.this,
                getResources().getString(R.string.delete_refused),
                getResources().getString(R.string.delete_refused_message),
                ErrorDialog.NEUTRAL_DIALOG).show();
    }

    /**
     * Refreshes the ListView by fetching all notes from local database. Furthermore it gets all
     * conflicts and a map which maps each note to the synchronized or not synchronized state and
     * initiates the appropriate visualisation.
     */
    private void updateListView() {
        SyncSQLHelper syncSQLHelper = SyncSQLHelper.getInstance(this, noteDatabaseProperties);
        List<Note> notes =  syncSQLHelper.getAllNotes();
        HashMap<Integer, Conflict> cidConflictMap = syncSQLHelper.getCidConflictMap();
        HashMap<Integer, Boolean> cidOfflineMap = syncSQLHelper.getCidOfflineMap();

        recyclerView.setAdapter(new RecyclerViewAdapter(notes, cidConflictMap, cidOfflineMap,
                getOnRecyclerItemSelectedListener(), this));
    }

    /**
     * Distinguishes between notes without conflicts and notes with MODIFICATION or PRESENCE
     * conflicts and retrieves a suitable listener for the short click.
     *
     * @return RecyclerViewListener with an Intent which redirects to ShowNoteActivity if the
     * selected note has no conflicts. Otherwise an RecyclerViewListener with an Intent which
     * redirects to ShowConflictActivity will be retrieved.
     */
    private RecyclerViewListener getOnRecyclerItemSelectedListener() {
        return new RecyclerViewListener() {
            @Override
            public void onClick(View view, Note note, Conflict conflict) {
                if (conflict == null) {
                    Intent intent = new Intent(ShowNoteDatabaseActivity.this, ShowNoteActivity.class);
                    intent.putExtra(ShowNoteActivity.EXTRA_OPTION_KEY,
                            ShowNoteActivity.EXTRA_OPTION_MODIFY);
                    intent.putExtra(NoteDatabaseProperties.EXTRA_KEY,
                            noteDatabaseProperties);
                    intent.putExtra(Note.EXTRA_KEY, note);
                    ShowNoteDatabaseActivity.this.startActivity(intent);

                } else {
                    if (conflict.getType() == Conflict.MODIFICATION) {
                        Intent intent = new Intent(ShowNoteDatabaseActivity.this,
                                ShowConflictActivity.class);
                        intent.putExtra(NoteDatabaseProperties.EXTRA_KEY, noteDatabaseProperties);
                        intent.putExtra(Conflict.EXTRA_KEY, conflict);
                        ShowNoteDatabaseActivity.this.startActivity(intent);

                    } else if (conflict.getType() == Conflict.PRESENCE) {
                        Intent intent = new Intent(ShowNoteDatabaseActivity.this,
                                ShowConflictActivity.class);
                        intent.putExtra(NoteDatabaseProperties.EXTRA_KEY, noteDatabaseProperties);
                        intent.putExtra(Conflict.EXTRA_KEY, conflict);
                        ShowNoteDatabaseActivity.this.startActivity(intent);
                    }
                }
            }
        };
    }
}
