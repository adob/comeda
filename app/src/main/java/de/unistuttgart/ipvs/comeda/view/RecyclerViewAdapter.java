package de.unistuttgart.ipvs.comeda.view;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Suitable adapter for a {@link RecyclerView} to list all available notes in a note database.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.NoteViewHolder> {

    private Context context;

    /**
     * Inner class to handle the onClick event calling onClick in {@link RecyclerViewListener}
     */
    public class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerViewListener viewListener;

        CardView noteCardView;
        TextView noteContent;
        TextView noteMetaData;
        TextView noteConflictData;
        ImageView noteSyncStateIcon;
        RelativeLayout noteConflictDataContainer;

        NoteViewHolder(View itemView, RecyclerViewListener viewListener) {
            super(itemView);
            this.viewListener = viewListener;
            this.noteCardView = itemView.findViewById(R.id.note_card_view);
            this.noteContent = itemView.findViewById(R.id.note_content);
            this.noteMetaData = itemView.findViewById(R.id.note_meta_data);
            this.noteConflictData = itemView.findViewById(R.id.note_conflict_data);
            this.noteSyncStateIcon = itemView.findViewById(R.id.sync_state_icon);
            this.noteConflictDataContainer = itemView.findViewById(R.id.note_conflict_data_container);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Note note = notes.get(getAdapterPosition());
            viewListener.onClick(view, note, cidConflictMap.get(note.getCid()));
        }
    }

    private List<Note> notes;
    private HashMap<Integer, Conflict> cidConflictMap;
    private HashMap<Integer, Boolean> cidOfflineMap;
    private RecyclerViewListener mouseListener;

    /**
     * @param notes List of {@link Note}s
     * @param cidConflictMap A mapping between <code>CID</code> values and {@link Conflict} objects
     * @param cidOfflineMap  A mapping between <code>CID</code> and Boolean values
     * @param mouseListener An object which implements the interface {@link RecyclerViewListener}
     * @param context {@link Context} of usage
     */
    public RecyclerViewAdapter(List<Note> notes, HashMap<Integer, Conflict> cidConflictMap,
                               HashMap<Integer, Boolean> cidOfflineMap,
                               RecyclerViewListener mouseListener, Context context) {
        this.notes = notes;
        this.cidConflictMap = cidConflictMap;
        this.cidOfflineMap = cidOfflineMap;
        this.mouseListener = mouseListener;
        this.context = context;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.note_card, parent, false);

        return new NoteViewHolder(view, mouseListener);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, final int position) {
        Note note = notes.get(position);

        // Extract the date from timestamp
        Date date = new Date((long) note.getTimestamp() * 1000);
        String formattedDate = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US).format(date);

        // Prepare combined text
        String metaData = new StringBuilder()
            .append(context.getResources().getString(R.string.last_modified))
            .append(": ")
            .append(formattedDate)
            .append("    ")
            .append(context.getResources().getString(R.string.version))
            .append(": ")
            .append(note.getVersion()).toString();

        // Set contents to appropriate UI elements
        holder.noteContent.setText(note.getContent());
        holder.noteMetaData.setText(metaData);


        // Set an OFFLINE or ONLINE icon image
        if (cidOfflineMap.get(note.getCid()) != null) {
            if (cidOfflineMap.get(note.getCid())) {
                holder.noteSyncStateIcon.setImageResource(R.drawable.ic_cloud_off_gray_48dp);

            } else {
                holder.noteSyncStateIcon.setImageResource(R.drawable.ic_cloud_gray_48dp);
            }
        }

        // If a conflict is present, prepare an extra text holder for the notification
        Conflict conflict = cidConflictMap.get(note.getCid());

        if (conflict != null) {
            switch (conflict.getType()) {
                case Conflict.MODIFICATION:
                    holder.noteConflictData.setText(
                            context.getResources().getString(R.string.change_conflict_label));
                    break;
                case Conflict.PRESENCE:
                    holder.noteConflictData.setText(
                            context.getResources().getString(R.string.presence_conflict_label));
                    break;
            }

            holder.noteConflictDataContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

}
