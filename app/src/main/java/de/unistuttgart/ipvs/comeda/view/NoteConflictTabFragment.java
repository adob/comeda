package de.unistuttgart.ipvs.comeda.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.unistuttgart.ipvs.comeda.R;


/**
 * Represents the content of a tab which is used by {@link ViewPagerAdapter}.
 */
public class NoteConflictTabFragment extends Fragment {

    private View view;
    private EditText editText;
    private String initialContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        view = inflater.inflate(R.layout.note_conflict_tab, viewGroup, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        editText = view.findViewById(R.id.conflict_note_edit);
        editText.setText(initialContent);
    }

    public void setInitialContent(String initialContent) {
        this.initialContent = initialContent;
    }

    public String getContent() {
        return editText.getText().toString();
    }
}
