package de.unistuttgart.ipvs.comeda.model;


/**
 * Provides all modes which can be assigned to a {@link Note} in the local database.
 */
public class Mode {

    public static final int NEW = 0;
    public static final int RELEASED = 1;
    public static final int MODIFIED = 2;
    public static final int DELETED = 3;
    public static final int OFFLINE = 4;
}
