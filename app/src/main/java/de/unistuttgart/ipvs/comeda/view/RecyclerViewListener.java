package de.unistuttgart.ipvs.comeda.view;


import android.view.View;

import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Used to give a possibility to define an OnClick listener in arbitrary class. This method
 * is called by {@link de.unistuttgart.ipvs.comeda.view.RecyclerViewAdapter.NoteViewHolder}.
 */
public interface RecyclerViewListener {

    void onClick(View view, Note note, Conflict conflict);
}
