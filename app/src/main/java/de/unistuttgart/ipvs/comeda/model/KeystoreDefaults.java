package de.unistuttgart.ipvs.comeda.model;


/**
 * Used to make values understandable for
 * {@link de.unistuttgart.ipvs.comeda.ssl.SecureClientConnector}.
 */
public class KeystoreDefaults {

    public static char[] getKeystorePassword() {
        return Constants.KEYSTORE_PASSWORD.toCharArray();
    }
}
