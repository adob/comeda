package de.unistuttgart.ipvs.comeda.stream;

import java.io.IOException;


/**
 * Wraps {@link StreamReader} and {@link StreamWriter} and ensures the correctness in data transfer.
 */
public class Stream {

    private static String ACK = "ack";

    private StreamReader reader;
    private StreamWriter writer;

    public Stream(StreamReader reader, StreamWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    /**
     * Sends a string to the stream using {@link StreamWriter}.
     *
     * @param data Data which should be sent
     * @throws IOException if the receiver did not confirmed the receive by sending back an ACK.
     */
    public void write(String data) throws IOException {
        writer.write(data);

        if (!reader.read().equals(ACK)) {
            throw new IOException();
        }
    }

    /**
     * Receives a string from the stream using {@link StreamReader} and confirms the receive by
     * sending back an ACK.
     *
     * @return Received data
     */
    public String read() throws IOException {
        String data = reader.read();
        writer.write(ACK);
        return data;
    }

    /**
     * Closes all streams.
     */
    public void close() throws IOException {
        reader.close();
        writer.close();
    }
}
