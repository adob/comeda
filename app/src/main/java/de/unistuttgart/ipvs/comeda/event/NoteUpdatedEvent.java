package de.unistuttgart.ipvs.comeda.event;


import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Used in Activities to notify all synchronization components that a Note has been modified
 * in the local database.
 */
public class NoteUpdatedEvent extends NoteEvent {

    private Note note;


    public NoteUpdatedEvent(Note note) {
        this.note = note;
    }

    public Note getNote() {
        return note;
    }
}
