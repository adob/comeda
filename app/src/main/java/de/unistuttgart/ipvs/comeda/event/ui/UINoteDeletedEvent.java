package de.unistuttgart.ipvs.comeda.event.ui;

import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} to notify the UI of
 * {@link de.unistuttgart.ipvs.comeda.activities.ShowNoteActivity} about concurrent deletion.
 */
public class UINoteDeletedEvent {

    private Note deletedNote;

    public UINoteDeletedEvent(Note deletedNote) {
        this.deletedNote = deletedNote;
    }

    public Note getDeletedNote() {
        return deletedNote;
    }
}
