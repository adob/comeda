package de.unistuttgart.ipvs.comeda.event;


import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Used in Activities to notify all synchronization components that a new Note has been added to
 * the local database.
 */
public class NoteAddedEvent extends NoteEvent {

    private Note note;

    public NoteAddedEvent(Note note) {
        this.note = note;
    }

    public Note getNote() {
        return note;
    }
}
