package de.unistuttgart.ipvs.comeda.util;


import java.util.ArrayList;
import java.util.Map;

import de.unistuttgart.ipvs.comeda.automaton.Transition;
import de.unistuttgart.ipvs.comeda.model.Note;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;


/**
 * A helper to prepare data for sending over SSL sockets. {@link Parser} provides for each method
 * of {@link Composer} o method with reverse functionality.
 */
public class Composer {

    /**
     * @return Composed data line with appropriate delimiter
     */
    public static String composeDataLine(String line) {
        return "d/" + line;
    }

    /**
     * @param transition Transition to compose
     * @return Composed {@link Transition} with appropriate delimiter
     */
    public static String composeTransition(Transition transition) {
        return "t/" + transition.toString();
    }

    /**
     * @param properties {@link NoteDatabaseProperties} which should be composed
     * @return Composed {@link NoteDatabaseProperties}
     */
    public static String composeNoteDatabaseProperties(NoteDatabaseProperties properties) {
        return new StringBuilder()
                .append(properties.getHost())
                .append(":")
                .append(properties.getPort())
                .append(":")
                .append(properties.getDatabase())
                .append(":")
                .append(properties.getUsername())
                .append(":")
                .append(properties.getPassword()).toString();
    }

    /**
     * Composes an ArrayList of Integers by separating them with ':' as delimiter.
     *
     * @param vectorToCompose List of Integers to compose
     * @return composed Integer vector
     */
    public static String composeIntVector(ArrayList<Integer> vectorToCompose) {
        StringBuilder vectorBuilder = new StringBuilder("");

        for (int i = 0; i < vectorToCompose.size(); i++) {
            vectorBuilder.append(vectorToCompose.get(i));

            if (i < vectorToCompose.size() - 1 ) {
                vectorBuilder.append(":");
            }
        }

        return vectorBuilder.toString();
    }

    /**
     * @param note {@link Note} to compose
     * @return Composed {@link Note}
     */
    public static String composeNote(Note note) {
        return new StringBuilder()
                .append(note.getSid())
                .append(":")
                .append(note.getCid())
                .append(":")
                .append(note.getVersion())
                .append(":")
                .append(note.getTimestamp())
                .append(":")
                .append(note.getContent()).toString();
    }

    /**
     * Composes an Integer array by separating tuple values with ':' and tuples itself by a ';'.
     *
     * @param mapToCompose Map to be composed
     * @return Composed Integer-Integer map
     */
    public static String composeIntMap(Map<Integer, Integer> mapToCompose) {
        StringBuilder composedMap = new StringBuilder();

        int size = mapToCompose.entrySet().size();
        int i = 0;

        for (Map.Entry<Integer, Integer> sidCidEntry : mapToCompose.entrySet()) {
            int sid = sidCidEntry.getKey();
            int cid = sidCidEntry.getValue();

            composedMap.append(sid);
            composedMap.append(":");
            composedMap.append(cid);

            if (i < size - 1) {
                composedMap.append(";");
            }

            i++;
        }

        return composedMap.toString();
    }

    /**
     * @param valueToCompose Integer to be composed
     * @return composed Integer
     */
    public static String composeInt(int valueToCompose) {
        return String.valueOf(valueToCompose);
    }

    /**
     * Composes a Boolean value whereby <code>true</code> is projected to 1 and <code>false</code>
     * to 0.
     * @param valueToCompose Boolean to be composed
     * @return Composed Boolean
     */
    public static String composeBoolean(boolean valueToCompose) {
        if (valueToCompose) {
            return "1";
        }
        return "0";
    }

    /**
     * Converts an ArrayList of {@link Note}s to a new ArrayList of composed {@link Note}s.
     *
     * @param notes ArrayList which should be converted
     * @return ArrayList of composed {@link Note}s
     */
    public static ArrayList<String> getComposedNoteArray(ArrayList<Note> notes) {
        ArrayList<String> composedNoteArray = new ArrayList<>();
        for (Note note : notes) {
            composedNoteArray.add(composeNote(note));
        }
        return composedNoteArray;
    }
}
