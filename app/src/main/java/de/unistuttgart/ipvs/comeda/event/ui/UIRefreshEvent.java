package de.unistuttgart.ipvs.comeda.event.ui;


/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} to notify
 * {@link de.unistuttgart.ipvs.comeda.activities.ShowNoteDatabaseActivity} about a change in
 * the local database.
 */
public class UIRefreshEvent {
    /* no content */
}
