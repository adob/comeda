package de.unistuttgart.ipvs.comeda.view;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;


/**
 * Suitable Adapter for a {@link android.widget.ListView} to list available note databases.
 */
public class NoteDatabaseListAdapter extends ArrayAdapter<NoteDatabaseProperties> {

    private final Context context;
    private ArrayList<NoteDatabaseProperties> databasePropertiesList;

    public NoteDatabaseListAdapter(Context context, ArrayList<NoteDatabaseProperties> databasePropertiesList) {
        super(context, R.layout.database_list_item, databasePropertiesList);

        this.context = context;
        this.databasePropertiesList = databasePropertiesList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.database_list_item, parent, false);

        TextView entryNameLabel = itemView.findViewById(R.id.db_entry_name);
        TextView shortDescriptionLabel = itemView.findViewById(R.id.db_short_description);

        entryNameLabel.setText(databasePropertiesList.get(position).getName());
        shortDescriptionLabel.setText(databasePropertiesList.get(position).getHost());

        return itemView;
    }
}
