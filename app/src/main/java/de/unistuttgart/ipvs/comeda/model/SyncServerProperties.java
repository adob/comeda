package de.unistuttgart.ipvs.comeda.model;


import android.os.Parcel;
import android.os.Parcelable;


/**
 * Represents properties of a connection to the synchronization server.
 * The storing is handled by {@link de.unistuttgart.ipvs.comeda.sql.PropertiesSQLHelper}.
 */
public class SyncServerProperties implements Parcelable {

    public static final String EXTRA_KEY = "10nsl7vs8";

    private String host;
    private String port;

    public SyncServerProperties() { }

    public SyncServerProperties(String host, String port) {
        this.host = host;
        this.port = port;
    }

    private SyncServerProperties(Parcel in) {
        host = in.readString();
        port = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(host);
        dest.writeString(port);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SyncServerProperties> CREATOR = new Creator<SyncServerProperties>() {
        @Override
        public SyncServerProperties createFromParcel(Parcel in) {
            return new SyncServerProperties(in);
        }

        @Override
        public SyncServerProperties[] newArray(int size) {
            return new SyncServerProperties[size];
        }
    };

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public int getPortAsInt() {
        if (port.trim().isEmpty()) {
            return -1;
        } else {
            return Integer.parseInt(port);
        }
    }
}
