package de.unistuttgart.ipvs.comeda.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.model.SyncServerProperties;

/**
 * Used as a functional interface between {@link NoteDatabaseProperties},
 * {@link SyncServerProperties} and the local database system.
 */
public class PropertiesSQLHelper extends SQLiteOpenHelper {

    public PropertiesSQLHelper(Context context) {
        super(context, "properties.db", null, 1);
    }

    /**
     * Initializes the table structure of the local database when the application is executed
     * first time.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createDatabaseTableCommand = "CREATE TABLE DATABASE(" +
                "ID INTEGER PRIMARY KEY, " +
                "NAME TEXT, " +
                "HOST TEXT, " +
                "PORT TEXT, " +
                "DATABASE TEXT, " +
                "USERNAME TEXT, " +
                "PASSWORD TEXT" +
                ");";

        String createSyncSrvTableCommand = "CREATE TABLE SYNCSERV(" +
                "ID INTEGER PRIMARY KEY, " +
                "KEY TEXT, " +
                "VALUE TEXT" +
                ");";

        String insertDummyHost = "INSERT INTO SYNCSERV(KEY, VALUE) " +
                "VALUES(" +
                "'host', " +
                "NULL" +
                ");";

        String insertDummyPort = "INSERT INTO SYNCSERV(KEY, VALUE) " +
                "VALUES(" +
                "'port', " +
                "NULL" +
                ");";

        sqLiteDatabase.execSQL(createDatabaseTableCommand);
        sqLiteDatabase.execSQL(createSyncSrvTableCommand);
        sqLiteDatabase.execSQL(insertDummyHost);
        sqLiteDatabase.execSQL(insertDummyPort);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) { /* not supported */ }


    /**
     * Saves the given {@link NoteDatabaseProperties} object to the local database.
     * @param properties NoteDatabaseProperties which should be persistent
     */
    public void insertNoteDatabaseProperties(NoteDatabaseProperties properties) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("NAME", properties.getName());
        values.put("HOST", properties.getHost());
        values.put("PORT", properties.getPort());
        values.put("DATABASE", properties.getDatabase());
        values.put("USERNAME", properties.getUsername());
        values.put("PASSWORD", properties.getPassword());

        long id = sqLiteDatabase.insert("DATABASE", null, values);
        properties.setId((int) id);

        sqLiteDatabase.close();
    }

    /**
     * Updates the given {@link NoteDatabaseProperties} object in the local database.
     * @param properties NoteDatabaseProperties which should be updated
     */
    public void updateNoteDatabaseProperties(NoteDatabaseProperties properties) {
        String sql_query = "UPDATE DATABASE SET NAME = '" + properties.getName() + "', " +
                "HOST = '" + properties.getHost() + "', " +
                "PORT = '" + properties.getPort() + "', " +
                "DATABASE = '" + properties.getDatabase() + "', " +
                "USERNAME = '" + properties.getUsername() + "', " +
                "PASSWORD = '" + properties.getPassword() + "' " +
                "WHERE ID = " + properties.getId() +
                ";";

        executeSQL(sql_query);
    }

    /**
     * Deletes the given {@link NoteDatabaseProperties} object from the local database.
     * @param properties NoteDatabaseProperties which should be deleted
     */
    public void deleteNoteDatabaseProperties(NoteDatabaseProperties properties) {
        executeSQL("DELETE FROM DATABASE WHERE ID = " + properties.getId());
    }

    /**
     * @return All {@link NoteDatabaseProperties} as objects which are contained by the
     * local database.
     */
    public ArrayList<NoteDatabaseProperties> getAllNoteDatabaseProperties() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ArrayList<NoteDatabaseProperties> noteDatabasePropertiesList = new ArrayList<>();

        String sql_query = "SELECT ID, NAME, HOST, PORT, DATABASE, USERNAME, PASSWORD FROM DATABASE;";

        Cursor cursor = sqLiteDatabase.rawQuery(sql_query, null);

        if (cursor.moveToFirst()) {
            do {
                NoteDatabaseProperties noteDatabaseProperties =
                        new NoteDatabaseProperties(cursor.getInt(0), cursor.getString(1),
                                cursor.getString(2), cursor.getString(3), cursor.getString(4),
                                cursor.getString(5), cursor.getString(6));
                noteDatabasePropertiesList.add(noteDatabaseProperties);
            } while (cursor.moveToNext());
        }

        cursor.close();
        sqLiteDatabase.close();
        return noteDatabasePropertiesList;
    }

    /**
     * Updates the given {@link SyncServerProperties} object in the local database.
     * @param properties SyncServerProperties which should be updated
     */
    public void updateSyncServerProperties(SyncServerProperties properties) {
        String updateHostCommand = "UPDATE SYNCSERV SET VALUE = '" + properties.getHost() +
                "' WHERE KEY = 'host';";

        String updatePortCommand = "UPDATE SYNCSERV SET VALUE = '" + properties.getPort() +
                "' WHERE KEY = 'port';";

        executeSQL(updateHostCommand);
        executeSQL(updatePortCommand);
    }

    /**
     * @return SyncServerProperties which were present on the local database.
     */
    public SyncServerProperties getSyncServerProperties() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        SyncServerProperties syncServerProperties = new SyncServerProperties();

        String getHostCommand = "SELECT VALUE FROM SYNCSERV WHERE KEY = 'host';";
        String getPortCommand = "SELECT VALUE FROM SYNCSERV WHERE KEY = 'port';";

        Cursor cursor = sqLiteDatabase.rawQuery(getHostCommand, null);

        if (cursor.moveToFirst()) {
            syncServerProperties.setHost(cursor.getString(0));
        }

        cursor.close();
        cursor = sqLiteDatabase.rawQuery(getPortCommand, null);

        if (cursor.moveToFirst()) {
            syncServerProperties.setPort(cursor.getString(0));
        }

        cursor.close();
        sqLiteDatabase.close();
        return syncServerProperties;
    }

    /**
     * Executes an arbitrary SQL command on the local database.
     *
     * @param sqlCommand SQL command to execute
     */
    private void executeSQL(String sqlCommand) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(sqlCommand);
        sqLiteDatabase.close();
    }

    /**
     * Inspects whether a {@link NoteDatabaseProperties} instance is present on the local database.
     * @param properties NoteDatabaseProperties which presence should be checked
     * @return <code>true</code> if a property with the same name is already present, else
     * <code>false</code>.
     */
    public boolean doesNotedatabasePropertiesExist(NoteDatabaseProperties properties) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String name = properties.getName().toLowerCase();
        String sql_query = "SELECT * FROM DATABASE WHERE LOWER(NAME) = '" + name + "';";
        Cursor cursor = sqLiteDatabase.rawQuery(sql_query, null);

        boolean exist = cursor.moveToFirst();
        cursor.close();
        return exist;
    }

}
