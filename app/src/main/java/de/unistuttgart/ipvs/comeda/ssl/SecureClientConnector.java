package de.unistuttgart.ipvs.comeda.ssl;


import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import de.unistuttgart.ipvs.comeda.model.Constants;
import de.unistuttgart.ipvs.comeda.model.SecureConnectionProperties;


/**
 * Organizes a secure connection using the TLS protocol according to
 * {@link SecureConnectionProperties}.
 */
public class SecureClientConnector {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private SSLSocket sslSocket;
    private SecureConnectionProperties secureConnectionProperties;

    public SecureClientConnector(SecureConnectionProperties secureConnectionProperties) {
        this.secureConnectionProperties = secureConnectionProperties;
    }

    /**
     * Establishes a secure connection to the sync server according to already set
     * {@link SecureConnectionProperties} by setting up a new sslSocket which can be
     * get from this object.
     */
    public void connect() throws KeyStoreException, CertificateException, NoSuchAlgorithmException,
            IOException, KeyManagementException {

        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(
                secureConnectionProperties.getKeystore(),
                secureConnectionProperties.getKeystorePassword());

        TrustManagerFactory trustManagerFactory =
                TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());

        SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        sslSocket = (SSLSocket) sslSocketFactory.createSocket(
                secureConnectionProperties.getHost(),
                secureConnectionProperties.getPort());

        sslSocket.setSoTimeout(10000);

        LOGGER.log(Level.INFO, "Connected to: " + sslSocket.getInetAddress());
    }

    /**
     * Blocks until a sslSocket has been set by <code>connect()</code>.
     */
    public void awaitSecureConnection() {
        while (sslSocket == null) {
            try {
                Thread.sleep(1000); // wait until a secure connection is established.
            } catch (InterruptedException e) { /* do nothing */ }
        }
    }

    public SSLSocket getSslSocket() {
        return sslSocket;
    }
}
