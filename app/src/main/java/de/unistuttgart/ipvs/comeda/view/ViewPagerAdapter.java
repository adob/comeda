package de.unistuttgart.ipvs.comeda.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Suitable adapter for {@link ViewPager} which contains both {@link NoteConflictTabFragment}s.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private int tabCount;

    private NoteConflictTabFragment localNoteTabFragment;
    private NoteConflictTabFragment remoteNoteTabFragment;

    public ViewPagerAdapter(FragmentManager fragmentManager, int tabCount,
                            Note localConflictNote, Note remoteConflictNote) {
        super(fragmentManager);
        this.tabCount = tabCount;

        this.localNoteTabFragment = new NoteConflictTabFragment();
        this.remoteNoteTabFragment = new NoteConflictTabFragment();

        localNoteTabFragment.setInitialContent(localConflictNote.getContent());
        remoteNoteTabFragment.setInitialContent(remoteConflictNote.getContent());
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return localNoteTabFragment;
            case 1:
                return remoteNoteTabFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
