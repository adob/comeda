package de.unistuttgart.ipvs.comeda.model;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Contains all necessary data which is required for a secure connection to the sync server.
 */
public class SecureConnectionProperties {
    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private static final String KEYSTORE_FILE = "/keyStore";

    private String host;
    private int port;
    private File keyStore;
    private char[] keystorePassword;

    public SecureConnectionProperties(String host, int port, InputStream keystore,
                                      char[] keystorePassword, String cachePath) {
        this.host = host;
        this.port = port;
        this.keystorePassword = keystorePassword;
        this.keyStore = new File(cachePath + KEYSTORE_FILE);
        setKeystore(keystore);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public char[] getKeystorePassword() {
        return keystorePassword;
    }

    /**
     * Stores the Keystore locally for further usage in
     * {@link de.unistuttgart.ipvs.comeda.ssl.SecureClientConnector}.
     * @param inputStream InputStream of the Keystore
     */
    private void setKeystore(InputStream inputStream) {
        int n;
        byte[] buffer = new byte[1024];
        try {
            FileOutputStream fos = new FileOutputStream(keyStore);
            while ((n = inputStream.read(buffer)) >= 0) {
                fos.write(buffer, 0, n);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }


    /**
     * @return The KeyStore as InputStream which has been set by the constructor.
     */
    public InputStream getKeystore() {
        try {
            return new FileInputStream(keyStore);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

}
