package de.unistuttgart.ipvs.comeda.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.sql.PropertiesSQLHelper;
import de.unistuttgart.ipvs.comeda.sync.SyncSQLHelper;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.util.Regex;
import de.unistuttgart.ipvs.comeda.view.ErrorDialog;


/**
 * An Activity which is used to add or modify note database properties.
 */
public class ShowNoteDatabasePropertiesActivity extends AppCompatActivity {

    public static final String EXTRA_OPTION_KEY = "extra_option";
    public static final int EXTRA_OPTION_ADD = 0;
    public static final int EXTRA_OPTION_MODIFY = 1;

    private NoteDatabaseProperties oldNoteDatabaseProperties;

    private int option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_note_database_properties);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        option = getIntent().getIntExtra(EXTRA_OPTION_KEY, EXTRA_OPTION_ADD);
        oldNoteDatabaseProperties = getIntent().getParcelableExtra(NoteDatabaseProperties.EXTRA_KEY);

        // The title for EXTRA_OPTION_ADD is set by default
        if (option == EXTRA_OPTION_MODIFY) {
            setTitle(getResources().getString(R.string.modify_db));
        }

        prepareInputFields();
    }

    /**
     * Inspects each field whether it's content matches the requirement or not.
     *
     * @return <code>true</code> if all requirements are met, else <code>false</code>.
     */
    public boolean checkInput() {
        EditText hostEditText = findViewById(R.id.host_input);
        EditText portEditText = findViewById(R.id.port_input);
        EditText databaseEditText = findViewById(R.id.database_input);
        EditText usernameEditText = findViewById(R.id.username_input);

        String host = hostEditText.getText().toString().trim();
        String port = portEditText.getText().toString().trim();
        String database = databaseEditText.getText().toString().trim();
        String username = usernameEditText.getText().toString().trim();


        if (!Regex.isValidHostname(host) && !Regex.isValidIPAddress(host)) {
            showInputErrorDialog(getResources().getString(R.string.add_db_host), host);
            return false;

        } else if (!Regex.isPort(port)) {
            showInputErrorDialog(getResources().getString(R.string.add_db_port), port);
            return false;

        } else if (!Regex.isNormalizedName(database)) {
            showInputErrorDialog(getResources().getString(R.string.add_db_database), database);
            return false;

        } else if (!Regex.isNormalizedName(username)) {
            showInputErrorDialog(getResources().getString(R.string.add_db_username), username);
            return false;
        }

        return true;
    }

    /**
     * Generates a new {@link NoteDatabaseProperties} object from current user input data.
     *
     * @return Generated {@link NoteDatabaseProperties}
     */
    private NoteDatabaseProperties getNewNoteDatabasePropertiesFromEditFields() {
        EditText nameEditText = findViewById(R.id.name_input);
        EditText hostEditText = findViewById(R.id.host_input);
        EditText portEditText = findViewById(R.id.port_input);
        EditText databaseEditText = findViewById(R.id.database_input);
        EditText usernameEditText = findViewById(R.id.username_input);
        EditText passwordEditText = findViewById(R.id.password_input);

        String name = nameEditText.getText().toString().trim();
        String host = hostEditText.getText().toString().trim();
        String port = portEditText.getText().toString().trim();
        String database = databaseEditText.getText().toString().trim();
        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString();

        return new NoteDatabaseProperties(name, host, port, database, username, password);
    }

    /**
     * Inserts the generated {@link NoteDatabaseProperties} to the local database of note database
     * properties.
     */
    public void addDatabaseProperties() {
        NoteDatabaseProperties newProperties = getNewNoteDatabasePropertiesFromEditFields();
        PropertiesSQLHelper propertiesSQLHelper = new PropertiesSQLHelper(this);

        if (!propertiesSQLHelper.doesNotedatabasePropertiesExist(newProperties)) {
            propertiesSQLHelper.insertNoteDatabaseProperties(newProperties);
            SyncSQLHelper.getInstance(this, newProperties).createTables();
            finish();

        } else {
            showErrorDialog(String.format(getResources().getString(R.string.error_note_exists),
                            newProperties.getName()) +
                    "\n\n" + getResources().getString(R.string.error_choose_another_name));
        }
    }

    /**
     * Updates {@link NoteDatabaseProperties} on the local database of note database properties.
     */
    private void updateDatabaseProperties() {
        NoteDatabaseProperties newNoteDatabaseProperties = getNewNoteDatabasePropertiesFromEditFields();
        PropertiesSQLHelper propertiesSQLHelper = new PropertiesSQLHelper(this);

        newNoteDatabaseProperties.setId(oldNoteDatabaseProperties.getId());

        boolean oldNameMatches = oldNoteDatabaseProperties.getName().toLowerCase().equals(
                newNoteDatabaseProperties.getName().toLowerCase());

        if (oldNameMatches) {
            propertiesSQLHelper.updateNoteDatabaseProperties(newNoteDatabaseProperties);
            finish();

        } else if (!propertiesSQLHelper.doesNotedatabasePropertiesExist(newNoteDatabaseProperties)) {
            propertiesSQLHelper.updateNoteDatabaseProperties(newNoteDatabaseProperties);
            finish();

        } else {
            showErrorDialog(String.format(getResources().getString(R.string.error_note_exists),
                    newNoteDatabaseProperties.getName()) +
                    "\n\n" + getResources().getString(R.string.error_choose_another_name));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_modify_note_database, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isInputChanged()) {
                    showAskDialog();
                } else {
                    finish();
                }
                return true;
            case R.id.action_save_note_database_properties:
                if (checkInput()) {
                    if (option == EXTRA_OPTION_ADD) {
                        addDatabaseProperties();
                    } else if (option == EXTRA_OPTION_MODIFY) {
                        updateDatabaseProperties();
                    }
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            if (isInputChanged()) {
                showAskDialog();
            } else {
                finish();
            }
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    /**
     * Shows an dialog to get user's confirmation about discarding the changes which were made by
     * the user.
     */
    public void showAskDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle(getResources().getString(R.string.discard_changes));
        dialog.setMessage(getResources().getString(R.string.discard_changes_question));
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(android.R.string.yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            dialogInterface.dismiss();
                            finish();
                        }
                    }
                });

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(android.R.string.no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == DialogInterface.BUTTON_NEGATIVE) {
                            dialogInterface.dismiss();
                        }
                    }
                });
        dialog.show();
    }

    /**
     * @return <code>true</code> if the user has changed at least one field. Otherwise
     * <code>false</code>.
     */
    private boolean isInputChanged() {
        return (option == EXTRA_OPTION_ADD && !isInputEmpty()) ||
                (option == EXTRA_OPTION_MODIFY &&
                        !oldNoteDatabaseProperties.equals(
                                getNewNoteDatabasePropertiesFromEditFields()));
    }

    /**
     * Used when EXTRA_OPTION_MODIFY is set to load and show {@link NoteDatabaseProperties}.
     */
    private void prepareInputFields() {
        if (option == EXTRA_OPTION_MODIFY) {
            EditText nameEditText = findViewById(R.id.name_input);
            EditText hostEditText = findViewById(R.id.host_input);
            EditText portEditText = findViewById(R.id.port_input);
            EditText databaseEditText = findViewById(R.id.database_input);
            EditText usernameEditText = findViewById(R.id.username_input);
            EditText passwordEditText = findViewById(R.id.password_input);

            nameEditText.setText(oldNoteDatabaseProperties.getName());
            hostEditText.setText(oldNoteDatabaseProperties.getHost());
            portEditText.setText(oldNoteDatabaseProperties.getPort());
            databaseEditText.setText(oldNoteDatabaseProperties.getDatabase());
            usernameEditText.setText(oldNoteDatabaseProperties.getUsername());
            passwordEditText.setText(oldNoteDatabaseProperties.getPassword());
        }
    }

    /**
     * @return <code>true</code> if no inout was made by the user, else <code>false</code>.
     */
    private boolean isInputEmpty() {
        EditText nameEditText = findViewById(R.id.name_input);
        EditText hostEditText = findViewById(R.id.host_input);
        EditText portEditText = findViewById(R.id.port_input);
        EditText databaseEditText = findViewById(R.id.database_input);
        EditText usernameEditText = findViewById(R.id.username_input);
        EditText passwordEditText = findViewById(R.id.password_input);

        String name = nameEditText.getText().toString().trim();
        String host = hostEditText.getText().toString().trim();
        String port = portEditText.getText().toString().trim();
        String database = databaseEditText.getText().toString().trim();
        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString();

        return name.isEmpty() && host.isEmpty() && port.isEmpty() &&
                database.isEmpty() && username.isEmpty() && password.isEmpty();
    }

    /**
     * Shows a popup which identifies a field with a value which does not meet the requirements.
     *
     * @param field Affected field
     * @param value Affected value
     */
    private void showInputErrorDialog(String field, String value) {
        new ErrorDialog(this,
                getResources().getString(R.string.error_input),
                String.format(getResources().getString(R.string.error_input_message), value, field),
                ErrorDialog.NEUTRAL_DIALOG).show();
    }

    /**
     * Displays a main error with the given message.
     *
     * @param message Error message
     */
    private void showErrorDialog(String message) {
        new ErrorDialog(this, getResources().getString(R.string.error),
                message, ErrorDialog.NEUTRAL_DIALOG).show();
    }

}
