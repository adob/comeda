package de.unistuttgart.ipvs.comeda.event;


import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Used in Activities to notify all synchronization components that a Note has been marked
 * as DELETED in the local database.
 */
public class NoteDeletedEvent extends NoteEvent {

    private Note note;

    public NoteDeletedEvent(Note note) {
        this.note = note;
    }

    public Note getNote() {
        return note;
    }
}
