package de.unistuttgart.ipvs.comeda.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.event.NoteAddedEvent;
import de.unistuttgart.ipvs.comeda.event.NoteDeletedEvent;
import de.unistuttgart.ipvs.comeda.event.NoteUpdatedEvent;
import de.unistuttgart.ipvs.comeda.event.ui.UINoteDeletedEvent;
import de.unistuttgart.ipvs.comeda.model.Conflict;
import de.unistuttgart.ipvs.comeda.model.Mode;
import de.unistuttgart.ipvs.comeda.model.Note;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.sync.SyncSQLHelper;


/**
 * This activity contains elements which represent a note. Options to change, delete, save and
 * switch mode to work exclusively offline with a note are provided.
 */
public class ShowNoteActivity extends AppCompatActivity {

    public static final String EXTRA_OPTION_KEY = "extra_option";
    public static final int EXTRA_OPTION_ADD = 0;
    public static final int EXTRA_OPTION_MODIFY = 1;

    private int option;

    private Note note;

    private SyncSQLHelper syncSQLHelper;
    private EventBus eventBus;

    private boolean synced;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_note);
        setTitle(getResources().getString(R.string.note));

        Toolbar toolbar = findViewById(R.id.note_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        option = getIntent().getIntExtra(EXTRA_OPTION_KEY, EXTRA_OPTION_ADD);
        note = getIntent().getParcelableExtra(Note.EXTRA_KEY);

        NoteDatabaseProperties noteDatabaseProperties =
                getIntent().getParcelableExtra(NoteDatabaseProperties.EXTRA_KEY);
        syncSQLHelper = SyncSQLHelper.getInstance(this, noteDatabaseProperties);

        eventBus = EventBus.getDefault();
        // register on EventBus to receive events from SyncProcessor
        eventBus.register(this);

        synced = true;

        setSyncState();
        setContents();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_show_note, menu);
        return true;
    }

    private void setSyncState() {
        // synced is true by default if EXTRA_OPTION_ADDED is set
        if (option == EXTRA_OPTION_MODIFY) {
            int mode = syncSQLHelper.getModeByCid(note);
            synced = (mode != Mode.OFFLINE);
        }
    }

    /**
     * Sets suitable icons for both synchronization states.
     */
    private void setSyncIconToggleState(MenuItem menuItem) {
        menuItem.setChecked(synced);
        if (synced) {
            menuItem.setIcon(R.drawable.ic_cloud_white_48dp);
            menuItem.setTitle(R.string.unsync_note);

        } else {
            menuItem.setIcon(R.drawable.ic_cloud_off_white_48dp);
            menuItem.setTitle(R.string.sync_note);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        setSyncIconToggleState(menu.findItem(R.id.action_toggle_sync));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                save();
                return true;

            case R.id.action_delete_note:
                drop();
                return true;

            case R.id.action_toggle_sync:
                synced = !synced;
                setSyncIconToggleState(item);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
            save();
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }

    /**
     * Requests user's confirmation and drops the shown note.
     */
    private void drop() {
        new AlertDialog.Builder(this)
        .setMessage(getResources().getString(R.string.delete_note_conf))
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // A note exists in internal database only when EXTRA_OPTION_MODIFY is set.
                // Otherwise changes will be ignored.
                if (option == EXTRA_OPTION_MODIFY) {
                    deleteNote();
                }
                finish();
            }
        })
        .setNegativeButton(android.R.string.no, null)
        .show();
    }

    /**
     * Distinguished between four cases according to the option and sync state and updated the note
     * which is shown.
     */
    private void save() {
        if (synced && option == EXTRA_OPTION_ADD) {
            insertNote(Mode.NEW);

        } else if (synced && option == EXTRA_OPTION_MODIFY) {
            int mode = syncSQLHelper.getModeByCid(note);
            if (mode == Mode.OFFLINE) {
                syncNote();

            } else {
                updateNote();
            }

        } else if (!synced && option == EXTRA_OPTION_ADD) {
            insertNote(Mode.OFFLINE);

        } else if (!synced && option == EXTRA_OPTION_MODIFY) {
            int mode = syncSQLHelper.getModeByCid(note);
            if (mode == Mode.OFFLINE) {
                updateOfflineNote();

            } else {
                unsyncNote();
            }
        }

        finish();
    }

    /**
     * Updates the modified note which is in OFFLINE mode on local database system.
     */
    private void updateOfflineNote() {
        EditText contentEditText = findViewById(R.id.note_edit);
        String newContent = contentEditText.getText().toString().trim();
        int timestamp = (int) (System.currentTimeMillis() / 1000L);

        if (note.getContent().equals(newContent)) {
            return;
        }

        note.setContent(newContent);
        note.setTimestamp(timestamp);
        note.setVersion(syncSQLHelper.getVersionByCid(note.getCid()) + 1);
        syncSQLHelper.updateNoteDataByCid(note, Mode.OFFLINE);
    }

    /**
     * Synchronizes the note which was in OFFLINE mode by simulating the addition of a new note
     * on on local database system.
     */
    private void syncNote() {
        EditText contentEditText = findViewById(R.id.note_edit);
        String newContent = contentEditText.getText().toString().trim();
        int timestamp = (int) (System.currentTimeMillis() / 1000L);

        if (!note.getContent().equals(newContent)) {
            note.setContent(newContent);
            note.setTimestamp(timestamp);
            note.setVersion(syncSQLHelper.getVersionByCid(note.getCid()) + 1);
        }

        syncSQLHelper.updateNoteDataByCid(note, Mode.NEW);
        eventBus.post(new NoteAddedEvent(note));
    }

    /**
     * Switches the mode of a note to OFFLINE under consideration that changes may happen in the
     * background introduced by {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor}.
     * In that case a new conflict will be inserted.
     */
    private void unsyncNote() {
        EditText contentEditText = findViewById(R.id.note_edit);
        String newContent = contentEditText.getText().toString().trim();
        int timestamp = (int) (System.currentTimeMillis() / 1000L);

        // check for conflicts
        Note currentPersistentNote = syncSQLHelper.getNoteByCid(note.getCid());
        if (currentPersistentNote.getVersion() > note.getVersion()) {
            note.setContent(newContent);
            note.setTimestamp(timestamp);
            syncSQLHelper.insertConflict(note, Conflict.MODIFICATION);
            return;
        }

        if (!note.getContent().equals(newContent)) {
            note.setContent(newContent);
            note.setTimestamp(timestamp);
            note.setVersion(syncSQLHelper.getVersionByCid(note.getCid()) + 1);
        }

        syncSQLHelper.updateNoteDataByCid(note, Mode.OFFLINE);
        eventBus.post(new NoteDeletedEvent(note));
    }

    /**
     * Takes the current data and generates a new note which is inserted on local database system.
     *
     * @param mode Mode which should be set with the note
     */
    private void insertNote(int mode) {
        EditText contentEditText = findViewById(R.id.note_edit);
        String content = contentEditText.getText().toString().trim();

        // do not insert empty notes
        if (content.isEmpty()) {
            return;
        }

        int timestamp = (int) (System.currentTimeMillis() / 1000L);

        Note newNote = new Note(timestamp, content);

        int cid = syncSQLHelper.insertNote(newNote, mode);
        newNote.setCid(cid);

        // exclude sync
        if (mode != Mode.OFFLINE) {
            eventBus.post(new NoteAddedEvent(newNote));
        }
    }

    /**
     * Used as an option to reinsert a note which has been deleted by
     * {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} in background.
     */
    private void recoverNote() {
        EditText contentEditText = findViewById(R.id.note_edit);
        String content = contentEditText.getText().toString().trim();
        int timestamp = (int) (System.currentTimeMillis() / 1000L);

        Note recoveryNote = new Note(timestamp, content);
        recoveryNote.setVersion(note.getVersion());

        int cid = syncSQLHelper.insertNote(recoveryNote, Mode.NEW);
        recoveryNote.setCid(cid);
        note = recoveryNote;

        eventBus.post(new NoteAddedEvent(recoveryNote));
    }

    /**
     * Updates the note with current values dependant on it's mode according to the synchronization
     * protocol.
     *
     * NEW notes are updated but the mode remains unchanged.
     * RELEASED notes are updated, the version is increased and the mode is changed to MODIFIED.
     * MODIFIED notes are updated, whereby the mode remains unchanged.
     *
     * For the case that a note has been updated in the background, a conflict check is performed.
     */
    private void updateNote() {
        EditText contentEditText = findViewById(R.id.note_edit);
        String content = contentEditText.getText().toString().trim();

        // No changes are present
        if (content.equals(note.getContent())) {
            return;
        }

        note.setContent(content);
        note.setTimestamp((int) (System.currentTimeMillis() / 1000L));

        // Check for conflicts
        Note currentPersistentNote = syncSQLHelper.getNoteByCid(note.getCid());
        if (currentPersistentNote.getVersion() > note.getVersion()) {
            syncSQLHelper.insertConflict(note, Conflict.MODIFICATION);
            return;
        }

        int mode = syncSQLHelper.getModeByCid(note);

        if (mode == Mode.NEW) {
            syncSQLHelper.updateNoteDataByCid(note, Mode.NEW);
            eventBus.post(new NoteUpdatedEvent(note));

        } else if (mode == Mode.RELEASED) {
            note.setVersion(syncSQLHelper.getVersionByCid(note.getCid()) + 1);
            syncSQLHelper.updateNoteDataByCid(note, Mode.MODIFIED);
            eventBus.post(new NoteUpdatedEvent(note));

        } else if (mode == Mode.MODIFIED) {
            syncSQLHelper.updateNoteDataByCid(note, Mode.MODIFIED);
            eventBus.post(new NoteUpdatedEvent(note));
        }
    }

    /**
     * Deletes the opened note dependant on it's mode. OFFLINE notes are deleted immediately from
     * the local database. Notes in other modes are marked as deleted to match the synchronization
     * protocol.
     */
    private void deleteNote() {
        int mode = syncSQLHelper.getModeByCid(note);

        if (mode == Mode.MODIFIED) {
            // In this mode the version has been increased by 1.
            // Decrease local version to prevent lost update on the server when another client
            // increases the version by 1.
            int version = syncSQLHelper.getVersionByCid(note.getCid());
            syncSQLHelper.updateVersionByCid(note.getCid(), version - 1);
        }

        if (mode == Mode.OFFLINE) {
            syncSQLHelper.deleteNote(note);

        } else {
            syncSQLHelper.markAsDeleted(note);
            eventBus.post(new NoteDeletedEvent(note));
        }
    }

    /**
     * Initializes the note content which is viewed by this activity.
     * */
    private void setContents() {
        if (option == EXTRA_OPTION_MODIFY) {
            EditText contentEditText = findViewById(R.id.note_edit);
            contentEditText.setText(note.getContent());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listen(UINoteDeletedEvent event) {
        if (event.getDeletedNote().getCid() == note.getCid()) {
            showDeleteNotification();
        }
    }

    /**
     * Used for a popup notification in cases where a note has been deleted by
     * {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} while editing.
     *
     * Similar to PRESENCE conflict the note can be kept or deleted.
     */
    private void showDeleteNotification() {
        DialogInterface.OnClickListener keepListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == DialogInterface.BUTTON_POSITIVE) {
                    recoverNote();
                    dialogInterface.dismiss();
                }
            }
        };

        DialogInterface.OnClickListener deleteListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == DialogInterface.BUTTON_NEGATIVE) {
                    dialogInterface.dismiss();
                    finish();
                }
            }
        };

        android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this).create();
        dialog.setTitle(getResources().getString(R.string.presence_conflict));
        dialog.setCancelable(false);
        dialog.setMessage(getResources().getString(R.string.note_na_message));
        dialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE,
                getResources().getString(R.string.keep), keepListener);
        dialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.delete), deleteListener);
        dialog.show();
    }
}
