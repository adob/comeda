package de.unistuttgart.ipvs.comeda.sync;


import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Used to be enqueued in a {@link java.util.concurrent.ConcurrentLinkedQueue} within
 * {@link SyncProcessor}. It contains the {@link Note} which is affected
 * by this set operation.
 */
public class SyncJob {

    public enum Operation { ADD, UPDATE, DELETE }

    private Note note;
    private Operation operation;


    SyncJob(Note note, Operation operation) {
        this.note = note;
        this.operation = operation;
    }

    public Note getNote() {
        return note;
    }

    Operation getOperation() {
        return operation;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof SyncJob) {
            SyncJob otherSyncJob = (SyncJob) other;
            return note.equals(otherSyncJob.getNote()) && operation == otherSyncJob.getOperation();
        }
        return false;
    }
}
