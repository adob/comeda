package de.unistuttgart.ipvs.comeda.util;


/**
 * Contains helping static methods to inspect the formatting of values like IP addresses.
 */
public class Regex {
    public static boolean isValidHostname(String value) {
        String regex = "^(([a-z0-9]|[a-z0-9][a-z0-9\\-]*[a-z0-9])\\.)*([a-z0-9]|[a-z0-9][a-z0-9\\-]*[a-z0-9])$";
        return value.matches(regex);
    }

    public static boolean isValidIPAddress(String value) {
        String regex = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        return value.matches(regex);
    }

    public static boolean isNormalizedName(String value) {
        String regex = "^[a-zA-Z]+[a-zA-Z0-9]*$";
        return value.matches(regex);
    }

    public static boolean isPort(String value) {
        String regex = "^\\d{1,5}$";
        return value.matches(regex);
    }
}
