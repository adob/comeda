package de.unistuttgart.ipvs.comeda.util;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.unistuttgart.ipvs.comeda.model.Constants;


/**
 * Contains helping methods which affect the networking of the mobile device.
 */
public class Networking {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    /**
     * Inspects whether networking is available by pinging the public server 8.8.8.8
     *
     * @return <code>true</code> if a network connection is available, else <code>false</code>
     */
    public static boolean isNetworkAvailable() {
        Runtime runtime = Runtime.getRuntime();

        try {
            Process process = runtime.exec("/system/bin/ping -c 1 -w 3 8.8.8.8");
            int exit = process.waitFor();
            return exit == 0;

        } catch (IOException | InterruptedException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        return false;
    }
}
