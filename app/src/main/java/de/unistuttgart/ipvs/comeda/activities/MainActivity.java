package de.unistuttgart.ipvs.comeda.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import de.unistuttgart.ipvs.comeda.R;
import de.unistuttgart.ipvs.comeda.model.NoteDatabaseProperties;
import de.unistuttgart.ipvs.comeda.model.SyncServerProperties;
import de.unistuttgart.ipvs.comeda.sql.PropertiesSQLHelper;
import de.unistuttgart.ipvs.comeda.sync.SyncSQLHelper;
import de.unistuttgart.ipvs.comeda.view.NoteDatabaseListAdapter;

/**
 * This is the main entry point activity of this application. It displays all arranged note databases
 * and passes on other activities.
 */
public class MainActivity extends AppCompatActivity {

    PropertiesSQLHelper propertiesSQLHelper;

    /**
     * Initializes the layout, toolbars and the floating button.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.main_settings_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton addNoteDatabaseButton = findViewById(R.id.fab);
        addNoteDatabaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ShowNoteDatabasePropertiesActivity.class);

                // The same activity is used to modify or add a new note database.
                // This Extra defines in which mode the activity is going to be started.
                intent.putExtra(ShowNoteDatabasePropertiesActivity.EXTRA_OPTION_KEY,
                        ShowNoteDatabasePropertiesActivity.EXTRA_OPTION_ADD);

                MainActivity.this.startActivity(intent);
            }
        });

        propertiesSQLHelper = new PropertiesSQLHelper(this);
    }

    /**
     * Initializes the menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    /**
     * Called when a menu option is selected. It redirects to {@link MainSettingsActivity}.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, MainSettingsActivity.class);
            MainActivity.this.startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        NoteDatabaseListAdapter listAdapter = new NoteDatabaseListAdapter(this,
                propertiesSQLHelper.getAllNoteDatabaseProperties());

        ListView listView = findViewById(R.id.db_list_view);
        listView.setAdapter(listAdapter);
        setListener(listView);
    }

    /**
     * Sets up all essential listeners for the long click menu. It provides a forwarding to
     * {@link ShowNoteDatabasePropertiesActivity}. Furthermore a listener for selection and
     * deletion of a note * database is set.
     */
    private void setListener(final ListView listView) {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view,
                                           final int itemIndex, long l) {

                // The long click options menu is created within a popup.
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                // Use own adapter to display the name of the note database and some other meta data.
                final NoteDatabaseProperties properties =
                        ((NoteDatabaseListAdapter) listView.getAdapter()).getItem(itemIndex);


                builder.setTitle(properties != null ? properties.getName() : null);

                CharSequence[] options = new CharSequence[] {
                        getResources().getString(R.string.modify),
                        getResources().getString(R.string.delete)
                };

                builder.setItems(options,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int optionIndex) {
                                // Case modified option selected
                                if (optionIndex == 0) {
                                    Intent intent = new Intent(MainActivity.this,
                                            ShowNoteDatabasePropertiesActivity.class);

                                    intent.putExtra(
                                            ShowNoteDatabasePropertiesActivity.EXTRA_OPTION_KEY,
                                            ShowNoteDatabasePropertiesActivity.EXTRA_OPTION_MODIFY
                                    );

                                    intent.putExtra(NoteDatabaseProperties.EXTRA_KEY,
                                            properties);

                                    MainActivity.this.startActivity(intent);

                                // Case delete option selected
                                } else if (optionIndex == 1) {
                                    SyncSQLHelper syncSQLHelper = SyncSQLHelper.getInstance(
                                            MainActivity.this, properties);

                                    syncSQLHelper.deleteTables();
                                    propertiesSQLHelper.deleteNoteDatabaseProperties(properties);
                                    MainActivity.this.onResume();
                                }
                            }
                        }
                );

                builder.show();
                return true;
            }
        });

        // Listener for the selection of a note database (short click)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, ShowNoteDatabaseActivity.class);
                NoteDatabaseProperties noteDatabaseProperties =
                        ((NoteDatabaseListAdapter) listView.getAdapter()).getItem(i);

                PropertiesSQLHelper propertiesSQLHelper = new PropertiesSQLHelper(MainActivity.this);
                SyncServerProperties syncServerProperties = propertiesSQLHelper.getSyncServerProperties();

                intent.putExtra(SyncServerProperties.EXTRA_KEY, syncServerProperties);
                intent.putExtra(NoteDatabaseProperties.EXTRA_KEY, noteDatabaseProperties);

                MainActivity.this.startActivity(intent);
            }
        });
    }
}
