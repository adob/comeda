package de.unistuttgart.ipvs.comeda.view;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;


/**
 * Popup which displays an error message in desired {@link Context}.
 */
public class ErrorDialog {
    public static final int NEUTRAL_DIALOG = 0;

    private AlertDialog errorDialog;


    public ErrorDialog(Context context, String title, String message, int type) {
        this.errorDialog = new AlertDialog.Builder(context).create();
        this.errorDialog.setTitle(title);
        this.errorDialog.setMessage(message);
        prepareDialog(type);
    }

    /**
     * Prepares the listener and sets the available buttons dependant on the given type.
     *
     * @param type Type of the error dialog (E.g. <code>ErrorDialog.NEUTRAL_DIALOG</code>)
     */
    private void prepareDialog(int type) {
        switch (type) {
            case NEUTRAL_DIALOG:
                this.errorDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                break;
        }
    }

    /**
     * Pops the message up.
     */
    public void show() {
        this.errorDialog.show();
    }
}
