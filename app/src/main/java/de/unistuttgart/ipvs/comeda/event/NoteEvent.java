package de.unistuttgart.ipvs.comeda.event;


/**
 * Used as a superclass for {@link NoteUpdatedEvent}, {@link NoteAddedEvent} and
 * {@link NoteDeletedEvent}.
 */
public abstract class NoteEvent {
    /* used as a superclass */
}
