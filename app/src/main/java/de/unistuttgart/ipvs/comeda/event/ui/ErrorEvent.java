package de.unistuttgart.ipvs.comeda.event.ui;

/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.sync.SyncClient} to notify
 * {@link de.unistuttgart.ipvs.comeda.activities.ShowNoteDatabaseActivity} about errors e.g.
 * connection failure.
 */
public class ErrorEvent {

    private String title;
    private String message;

    public ErrorEvent(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public boolean equals(ErrorEvent other) {
        return other.getTitle().equals(title) && other.getMessage().equals(message);
    }
}
