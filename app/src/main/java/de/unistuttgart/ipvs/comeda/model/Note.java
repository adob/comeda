package de.unistuttgart.ipvs.comeda.model;


import android.os.Parcel;
import android.os.Parcelable;


/**
 * Represents a note which is saved in the local database system. It can also be delivered between
 * two Activities and transferred between the mobile device and the server using appropriate methods
 * from {@link de.unistuttgart.ipvs.comeda.util.Composer},
 * {@link de.unistuttgart.ipvs.comeda.util.Parser} and
 * {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor}.
 */
public class Note implements Parcelable {

    public static final String EXTRA_KEY = "j6vpnn0lxg";

    private int sid;
    private int cid;
    private int version;
    private int timestamp;
    private String content;


    public Note(int timestamp, String content) {
        this(0, 0, 1, timestamp, content);
    }

    public Note(int sid, int cid, int version, int timestamp, String content) {
        this.sid = sid;
        this.cid = cid;
        this.version = version;
        this.timestamp = timestamp;
        this.content = content;
    }

    protected Note(Parcel in) {
        this.sid = in.readInt();
        this.cid = in.readInt();
        this.version = in.readInt();
        this.timestamp = in.readInt();
        this.content = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sid);
        dest.writeInt(cid);
        dest.writeInt(version);
        dest.writeInt(timestamp);
        dest.writeString(content);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public int getSid() {
        return sid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getCid() {
        return cid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Note) {
            Note otherNote = (Note) other;
            return otherNote.cid == this.cid;
        }
        return false;
    }

}
