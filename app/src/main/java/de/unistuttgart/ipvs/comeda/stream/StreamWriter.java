package de.unistuttgart.ipvs.comeda.stream;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


/**
 * Handles the OutputStream which is retrieved from SSLSocket by writing the output.
 */
public class StreamWriter {

    private OutputStreamWriter outputStreamWriter;

    public StreamWriter(OutputStream outputStream) {
        this.outputStreamWriter = new OutputStreamWriter(outputStream);
    }

    /**
     * Builds and writes data packages by composing the header which contains the package size
     * and appropriate delimiter for the data. A package is defined by
     * "<code>p/(int) data_length/(char[]) data</code>".
     *
     * @param data Data to write to the stream
     */
    void write(String data) throws IOException {
        outputStreamWriter.write("p/" + data.length() + "/" + data);
        outputStreamWriter.flush();
    }

    void close() throws IOException {
        outputStreamWriter.close();
    }
}
