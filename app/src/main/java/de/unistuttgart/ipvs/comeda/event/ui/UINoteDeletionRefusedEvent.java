package de.unistuttgart.ipvs.comeda.event.ui;

import de.unistuttgart.ipvs.comeda.model.Note;


/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} to notify
 * {@link de.unistuttgart.ipvs.comeda.activities.ShowNoteDatabaseActivity} about displaying a
 * message informing the user that the last deletion has been refused.
 */
public class UINoteDeletionRefusedEvent {

    private Note note;

    public UINoteDeletionRefusedEvent(Note note) {
        this.note = note;
    }

    public Note getNote() {
        return note;
    }
}
