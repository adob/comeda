package de.unistuttgart.ipvs.comeda.event.ui;


/**
 * Thrown by {@link de.unistuttgart.ipvs.comeda.sync.SyncProcessor} to notify
 * {@link de.unistuttgart.ipvs.comeda.activities.ShowNoteDatabaseActivity} about displaying a
 * status message.
 */
public class UIInfoEvent {

    private String message;

    public UIInfoEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
