package de.unistuttgart.ipvs.comeda.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Handles the InputStream which is retrieved from SSLSocket by reading the input.
 */
public class StreamReader {

    private InputStreamReader inputStreamReader;

    public StreamReader(InputStream inputStream) {
        this.inputStreamReader = new InputStreamReader(inputStream);
    }

    /**
     * Reads data packages by parsing the header which contains the package size and appropriate
     * delimiter for the data. A package is defined by "<code>p/(int) data_length/(char[]) data</code>".
     *
     * @return Raw value which has been received from sender
     */
    public String read() throws IOException {

        char c = readChar();

        if (c != 'p') {
            throw new IOException("Unexpected header");
        }

        c = readChar();

        if (c != '/') {
            throw new IOException("Unexpected delimiter");
        }

        // parse data size
        StringBuilder sizeString = new StringBuilder();
        while ((c = readChar()) != '/') {
            sizeString.append(c);
        }

        int size;

        try {
            size = Integer.parseInt(sizeString.toString());

        } catch (NumberFormatException e) {
            throw new IOException("Unable to parse size: '" + sizeString + "'");
        }

        // parse data
        StringBuilder dataString = new StringBuilder();

        for (int i = 0; i < size; i++) {
            dataString.append(readChar());
        }

        return dataString.toString();
    }

    /**
     * Reads a single character from the inputStream.
     */
    private char readChar() throws IOException {
        char c = (char)inputStreamReader.read();

        if ((byte)c == -1) {
            throw new IOException("Stream reached the end");

        } else {
            return c;
        }
    }

    public void close() throws IOException {
        inputStreamReader.close();
    }
}
