package de.unistuttgart.ipvs.comeda.model;


/**
 * Contains different constants which may affect the whole application.
 */
public class Constants {

    public static String LOGGER_ID = "comeda.logger";
    public static String KEYSTORE_PASSWORD = "q1w2e3r4";

    public static class Titles {
        public static String CONNECTION_ERROR = "Connection error";
        public static String PROTOCOL_ERROR = "Protocol error";
    }


    public static class Messages {
        public static String CONNECTION_FAILED = "Unable to establish a secure connection to the desired server.\n\n" +
                "Please check your connection settings to the synchronization server.";
        public static String NO_NETWORK = "Please check your internet connection.";
        public static String PROTOCOL_VIOLATED = "The synchronization protocol has been violated:";
        public static String SYNC_STARTED = "Synchronization started.";
    }
}
